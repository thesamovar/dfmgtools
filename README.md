Various Python tools written by Dan Goodman. http://thesamovar.net/neuroscience

The structure of the package is like this:

* **core**
	For basic, simple utilities used in every type of project, e.g. file management, cacheing, etc.
* **compute**
	For scientific computation.
* **results**
	For presenting results of computations, e.g. plotting routines, automatic documentation, etc.
* **gui**
	For GUI tools.
* **matlab**
	For interfacing with Matlab. This isn't imported by default as it needs the mlabwrap
	module, which is fairly tricky to install (see the notes file in the package directory).

