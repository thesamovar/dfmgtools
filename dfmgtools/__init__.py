# This disables the SpikeQueue warning in Brian
import warnings

warnings.filterwarnings('ignore', category=UserWarning, message='.*SpikeQueue.*')

# This disables various codegen/experimental warnings in Brian
import logging

class _MessageFilter(logging.Filter):
    def __init__(self, matchstr):
        self.matchstr = matchstr
    def filter(self, record):
        return not self.matchstr in record.getMessage()

logging.getLogger('brian.reset').addFilter(_MessageFilter('codegen'))
logging.getLogger('brian.threshold').addFilter(_MessageFilter('codegen'))
logging.getLogger('brian.stateupdater').addFilter(_MessageFilter('codegen'))
logging.getLogger('brian.experimental.new_c_propagate').addFilter(_MessageFilter('new C'))

# We always import Brian and Brian Hears
from brian import *
from brian.hears import *

# Now we import various commonly used utils
from core import *
from compute import *
from results import *
