'''
Automatic adaptive gain control filterbank
'''

from brian import *
from brian.hears import *
from functools import partial
from dfmgtools.compute.auditory.level_and_amplitude import *

__all__ = ['AdaptiveGainControl', 'AdaptiveLevelControl',
           ]
        
class AdaptiveGainControl(CombinedFilterbank):
    '''
    Adaptive gain control.
    
    This AGC filterbank only makes sense on inputs that have been broken down
    into frequency components, e.g. by a `Gammatone` cochlear filterbank.
    
    It works as follows:
    
    1. We perform envelope extraction as follows:
       The input signal is squared, butterworth filtered
       with cutoff at 1/tau, multiplied by 2 and square rooted. See
       http://www.mathworks.com/help/dsp/examples/envelope-detection.html
       for more details.
    
    2. Given the envelope, we compute the gain necessary to apply to the signal
       to get the desired output envelope. Essentially, this is
       ``amplitude_io(x)/x`` except that we have to apply linear interpolation
       below a certain threshold in case ``amplitude_io(x)/x`` blows up at 0.
       
    3. Multiply this instantaneous gain with the input signal.
    
    You can also work with level instead of amplitude input/output functions,
    see `AdaptiveLevelControl`.
    
    Parameters
    ----------
    
    source : Filterbank
        The input to apply AGC to.
    amplitude_io : function
        A function ``f(amp)`` returning the desired adapted amplitude of a tone
        of amplitude ``amp``.
    threshold : float
        The AGC will be linear below this threshold amplitude.
    tau : Quantity
        The time constant of the gain control.
    '''
    def __init__(self, source, amplitude_io,
                 threshold=tone_amplitude_at(0*dB),
                 tau=5*ms,
                 rectified=False):
        CombinedFilterbank.__init__(self, source)
        source = self.get_modified_source()

        self.rectified = rectified
        self.tau = tau
        self.threshold = threshold
        self.amplitude_io = amplitude_io
        
        self.gainfunc = LinearisedGainFunction(amplitude_io, threshold)
        
        self.rect = rect = FunctionFilterbank(source, square)
        self.lpf = lpf = Butterworth(rect, rect.nchannels, 1, 1/tau)
        if rectified:
            envfunc = env_from_lpf_rectified
        else:
            envfunc = env_from_lpf
        self.envelope = env = FunctionFilterbank(lpf, envfunc)
        self.instantgain = instantgain = FunctionFilterbank(env, self.gainfunc)
        self.compressed = compressed = FunctionFilterbank((source, instantgain), multiply)

        self.set_output(compressed)


def env_from_lpf(x):
    return sqrt(2*x)

def env_from_lpf_rectified(x):
    return 2*sqrt(x)


class LinearisedFunction(object):
    '''
    Returns a version of the function f linearised in [0, xmin].
    
    The linearisation maintains continuity but not smoothness at
    (xmin, f(xmin)). The function should only be defined on [0, inf).
    '''
    def __init__(self, f, xmin):
        self.f = f
        self.xmin = xmin
        self.ymin = f(xmin)
        self.gradient = self.ymin/self.xmin
    def __call__(self, x):
        if shape(x)==():
            if x<self.xmin:
                return self.gradient*x
            else:
                return self.f(x)
        y = zeros_like(x)
        I = x>self.xmin
        y[I] = self.f(x[I])
        y[-I] = self.gradient*x[-I]
        return y
    
    
class LinearisedGainFunction(LinearisedFunction):
    '''
    Returns the gain of the function f linearised in [0, xmin].
    
    The linearisation maintains continuity but not smoothness of f at
    (xmin, f(xmin)). The function should only be defined on [0, inf).
    
    The function is a gain function in the sense that if
    ``g = LinearisedGainFunction(f, xmin)`` and ``h = LinearisedFunction(f, xmin)``
    then ``g(x)*x==h(x)``. This relationship is the reason for linearising
    around 0.
    '''
    def __call__(self, x):
        if shape(x)==():
            if x<self.xmin:
                return self.gradient
            else:
                return self.f(x)/x
        y = zeros_like(x)
        I = x>self.xmin
        xI = x[I]
        y[I] = self.f(xI)/xI
        y[-I] = self.gradient
        return y


class AmplitudeIO(object):
    '''
    An amplitude IO function from a level IO function.
    
    Use this with `AdaptiveGainControl`.
    
    Parameters
    ----------
    
    io_dB : function
        Should return the desired output level for a given input level.
    '''
    def __init__(self, io_dB):
        self.io_dB = io_dB
    def __call__(self, x):
        return tone_amplitude_at(self.io_dB(level_of_tone_with_amplitude(x)))


class AdaptiveLevelControl(AdaptiveGainControl):
    '''
    Like AdaptiveGainControl but control via level rather than amplitude
    
    Parameters
    ----------
    
    source : Filterbank
        The input to apply AGC to.
    level_io : function
        A function ``f(level)`` returning the desired output level given an
        input level.
    threshold : dB_type
        The AGC will be linear below this threshold level.
    tau : Quantity
        The time constant of the gain control.
    rectified : bool
        If the input signal is rectified, multiply envelope by sqrt(2) to compensate.
    '''
    def __init__(self, source, level_io, threshold=0*dB, tau=5*ms):
        amplitude_io = AmplitudeIO(level_io)
        threshold = tone_amplitude_at(threshold)
        AdaptiveGainControl.__init__(self, source, amplitude_io, threshold, tau)

if __name__=='__main__':
    if 0:
        x = linspace(0, 1, 100)
        f = lambda x: x**3
        xmin = 0.5
    
        g = LinearisedFunction(f, xmin)
        h = LinearisedGainFunction(f, xmin)
        
        subplot(311)
        
        plot(x, f(x))
        plot(x, g(x))
        
        subplot(312)
        
        plot(x, f(x)/x)
        plot(x, h(x))
        
        subplot(313)
        plot(x, f(x))
        plot(x, g(x))
        plot(x, h(x)*x, '--')

    if 0:
        print (tone_amplitude_at(30*dB),
               amax(array(tone(1*kHz, 100*ms).atlevel(30*dB))))
        
        print (level_of_tone_with_amplitude(tone_amplitude_at(30*dB)), 30*dB)
        
        print (level_of_tone_with_amplitude(0.1),
               (tone(1*kHz, 100*ms)*0.1).level)

    if 1:
        sound = sequence(
            tone(750*Hz, 50*ms).ramped(when='both', duration=1*ms).atlevel(40*dB),
            tone(750*Hz, 50*ms).ramped(when='both', duration=1*ms).atlevel(30*dB),
            ).shifted(10*ms)[:120*ms]
        rectified = False
        
        if rectified:
            sound = clip(sound, 0, Inf)
            
        func = lambda x: (1e10*x)**0.2
        
        threshold = 1e-10
        
        agc = AdaptiveGainControl(sound, func, threshold, 10*ms,
                                  rectified=rectified)

        subplot(211)
        plot(sound.times, sound)
        plot(sound.times, agc.envelope.process())
        subplot(212)
        plot(sound.times, agc.process())
        
    if 0:
        levels = linspace(0, 90, 100)
        io_dB = lambda lev: 45*(arctan((asarray(lev)-45.)/10.)-arctan(-4.5))/(2*arctan(4.5))
        threshold = 10*dB
        outlevels = []
        for level in levels:
            level = float(level)*dB
            sound = tone(1*kHz, 100*ms).ramped(when='both', duration=1*ms).atlevel(level)
            agc = AdaptiveLevelControl(sound, io_dB, threshold, 10*ms)
            soundout = Sound(agc.process())
            outlevels.append(soundout.level)
        plot(levels, io_dB(levels))
        plot(levels, outlevels)
            
    show()
    