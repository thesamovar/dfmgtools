'''
Gain filterbank
'''

from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.level_and_amplitude import *

__all__ = ['GainFilterbank']

class GainFilterbank(Filterbank):
    '''
    Applies a gain in dB, either a single value or one per channel.
    
    Parameters
    ----------
    
    source : Bufferable
        Input to apply gain to.
    gain : dB_type, list, array
        The gain in dB to apply, or one gain for each channel.
    '''
    def __init__(self, source, gain):
        if shape(gain)==():
            factor = gain.gain()
        else:
            if len(shape(gain))!=1:
                raise ValueError("Can only process 0D or 1D gain array")
            if source.nchannels==1:
                source = RestructureFilterbank(source, len(gain))
            if len(gain)!=source.nchannels:
                raise ValueError("number of gain channels does not match source "
                                 "len(gain)=%s, source.nchannels=%s"%(
                                        len(gain), source.nchannels))
            factor = []
            for g in gain:
                if not isinstance(g, dB_type):
                    g = g*dB
                factor.append(g.gain())
        self.factor = array(factor)[newaxis, :]
        self.gain = gain
        Filterbank.__init__(self, source)
        
    def buffer_apply(self, input):
        return input*self.factor


if __name__=='__main__':
    sound = tone(1*kHz, 10*ms).atlevel(60*dB)
    fb = GainFilterbank(sound, [-10*dB, 0*dB, 10*dB])
    outsound = Sound(fb.process())
    plot(sound.times, sound)
    plot(outsound.times, outsound)
    print outsound.level-sound.level
    show()
    