'''
Implements Gram Schmidt method for producing sounds with a given interaural correlation
from Culling, Colburn and Spurchise 2001 "Interaural correlation sensitivity".
'''

from numpy import *
from brian import *
from brian.hears import *

__all__ = ['gram_schmidt', 'make_correlated', 'correlated_noise']

def gram_schmidt(left, right):
    '''
    Takes two sounds and produces a pair A, B which are orthogonalised, where A=left and B
    is a mixture of A and B.
    '''
    if not left.samplerate==right.samplerate:
        raise ValueError("left and right samplerates are different")
    A = asarray(left).flatten()
    B = asarray(right).flatten()
    
    A_rms = sqrt(mean(A**2))
    B_rms = sqrt(mean(B**2))
    rho_AB = mean(A*B)/(A_rms*B_rms)
    c = A_rms/B_rms*B-rho_AB*A
    B2 = c/sqrt(1-rho_AB**2)
    
    return Sound(A, samplerate=left.samplerate), Sound(B2, samplerate=right.samplerate)

def make_correlated(left, right, rho):
    '''
    Takes two sounds and produces a new pair A, B which have the given correlation rho. Here rho
    can be a scalar or an array of the same length as left and right to have a correlation
    varying over time.
    '''
    if isinstance(rho, ndarray) and rho.shape!=left.shape:
        rho.shape = left.shape
    A, B2 = gram_schmidt(left, right)
    M = rho*A+sqrt(1-rho**2)*B2
    return Sound([A, M], samplerate=left.samplerate)

def correlated_noise(duration, level, rho):
    '''
    Produces two correlated white noises.
    '''
    left = whitenoise(duration).atlevel(level)
    right = whitenoise(duration).atlevel(level)
    return make_correlated(left, right, rho) 


if __name__=='__main__':
    #for rho in linspace(0, 1, 10):
    #for rho in [0.0, 1.0]*5:
    #    correlated_noise(1*second, 55*dB, rho).play(sleep=True)
        
    duration = 10*second
    level = 55*dB
    F = 0.2*Hz
    
    left = whitenoise(duration).atlevel(level)
    right = whitenoise(duration).atlevel(level)
    rho = (0.5*(1-cos(2*pi*F*left.times)))
    
    sound = make_correlated(left, right, rho)
    sound.play()
    
    ax = gcf().add_subplot(211)
    plot(sound.times, sound)
    gcf().add_subplot(212, sharex=ax)
    plot(sound.times, rho)
    show()
    