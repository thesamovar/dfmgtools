import hashlib
import inspect

__all__ = ['NameIdDescriptionSourceHashable']

class NameIdDescriptionSourceHashable(object):
    '''
    Default hashing and equality (for cacheing)
    
    Hash changes if name, id, description or source code of class changes. Note
    that description should include all the relevant parameters. You can also
    create a class or function attribute 'extra_sources', and the source code
    of these will be added (recursively) to the hash, to include additional
    source code dependencies.
    '''
    @property
    def uid(self):
        return self.id+'_'+self.hash()
    def hash(self):
        '''
        Platform independent hash, a string returned by MD5, very few collisions
        '''
        m = hashlib.md5()
        m.update(self.name)
        m.update(self.id)
        m.update(self.description)
        sources = []
        def extend_sources(srcobj):
            sources.append(srcobj)
            if hasattr(srcobj, 'extra_sources'):
                for o in srcobj.extra_sources:
                    extend_sources(o)
        extend_sources(self.__class__)
        src = ''
        for srcobj in sources:
            try:
                src += inspect.getsource(srcobj)
            except IOError:
                pass
        m.update(src)
        return m.hexdigest()
    def __hash__(self):
        '''
        Platform dependent hash, int, may have collisions
        '''
        return hash(self.hash())
    def __eq__(self, other):
        return self.hash()==other.hash()
    def __lt__(self, other):
        return self.hash()<other.hash()
    def __le__(self, other):
        return self<other or self==other
    def __ge__(self, other):
        return other<self or self==other
    def __gt__(self, other):
        return other<self
    def __ne__(self, other):
        return not self==other
