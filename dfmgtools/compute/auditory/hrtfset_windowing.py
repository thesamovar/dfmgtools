from brian import *
from brian.hears import *
from scipy.signal import hann

__all__ = ['apply_adaptive_windowing']

def apply_adaptive_windowing(hrtfset, aw, reverse, ramptime, onset_threshold=.15, cutting=True):
    '''
    aw is the adaptive window length in samples
    reverse is how far back from the onset of the window to go
    ramptime is how quick the hann ramping should be
    onset_threshold*amax(impulse_response) is used as the threshold for the start of the window
    cutting changes the size of the impulse responses to fit the window size
    '''
    startmin = hrtfset.data.shape[2]
    startmax = 0
    for i in xrange(2):
        for j in xrange(hrtfset.num_indices):
            D = hrtfset.data[i, j, :]
            onset = min((abs(D)>amax(abs(D))*onset_threshold).nonzero()[0])-reverse
            if onset<startmin:
                startmin = onset
            if onset>startmax:
                startmax = onset
            D[:onset] = 0
            D[onset+aw:] = 0
            D[onset+aw-ramptime:onset+aw] *= hann(2*ramptime)[ramptime:]
            hrtfset.data[i, j, :] = D
    if cutting:
        hrtfset.data = hrtfset.data[:, :, startmin:startmax+aw].copy()
    hrtfset.hrtf = []
    for i in xrange(hrtfset.num_indices):
        l = Sound(hrtfset.data[0, i, :], samplerate=hrtfset.samplerate)
        r = Sound(hrtfset.data[1, i, :], samplerate=hrtfset.samplerate)
        hrtfset.hrtf.append(HRTF(l, r))
    return hrtfset
