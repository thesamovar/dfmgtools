'''
Convenience functions for converting between amplitudes and levels.
'''

from brian import *
from brian.hears import *

__all__ = ['tone_amplitude_at',
           'level_of_tone_with_amplitude',
           ]

tone_amplitude_at_const = log10(sqrt(0.5)/2e-5)
def tone_amplitude_at(level):
    '''
    Returns the amplitude of a tone at the given level in dB SPL
    '''
    #return 10**(float(level)/20)/(sqrt(0.5)/2e-5)
    return 10**(asarray(level)/20-tone_amplitude_at_const)


def level_of_tone_with_amplitude(amp):
    '''
    Returns the level in dB SPL of a tone with a given amplitude.
    '''
    return 20*log10(amp*sqrt(0.5)/2e-5)*dB
