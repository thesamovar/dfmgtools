from dfmgtools.core.func_cache import func_cache
from hashing import *

__all__ = ['Model']

class Model(NameIdDescriptionSourceHashable):
    name = 'No model'
    id = 'nomodel'
    description = 'No model'
    binaural = False
    _quickcached = False
    @property
    def parameters_table(self):
        if not hasattr(self, 'params'):
            return ''
        output = '<table border="1" cellspacing="0" cellpadding="5px">'
        for name, value in self.params.iteritems():
            output += '''
            <tr>
            <td>{name}</td>
            <td>{value}</td>
            </tr>
            '''.format(name=name, value=value)
        output += '</table>'
        return output
    def start(self):
        pass
    def end(self):
        pass
    def run(self, sound, repeats, cfs, params):
        return [], []
    def quickcache_run(self, *args, **kwds):
        kwds.pop('reporter', None)
        res, additional_res = quickcache(self, *args, **kwds)
        self.set_additional_results(additional_res)
        return res
    def quickcached(self):
        self._run = self.run
        self.run = self.quickcache_run
        self._quickcached = True
        return self
    def set_additional_results(self, additional_res):
        self._additional_res = additional_res
        for k, v in additional_res.items():
            setattr(self, k, v)
    def additional_results(self):
        if not hasattr(self, '_additional_res'):
            self._additional_res = {}
        return self._additional_res

@func_cache
def quickcache(model, *args, **kwds):
    res = model._run(*args, **kwds)
    additional_res = model.additional_results()
    return res, additional_res
