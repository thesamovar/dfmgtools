from brian import *
from brian.hears import *
from dfmgtools.core import *
from dfmgtools.compute.auditory import *
from dfmgtools.matlab.auditory import *
import os

__all__ = ['CatANMatlabModel',
           'cat_an_model',
           'CatANModel']

thisdir, _ = os.path.split(__file__)

class CatANMatlabModel(MatlabModel):
    
    def get_matlab_functions(self):
        with in_directory(thisdir):
            self.setup_matlab_function('catmodel_IHC', 1)
            self.setup_matlab_function('catmodel_Synapse', 2)
            
    multichannel = False
    maxblocksize = 64
    
    def __call__(self, sound, cfs, cohc=1., cihc=1., fibertype=3., implnt=0.):
        if not sound.nchannels==1:
            raise ValueError('Cat AN model does not support multichannel sounds.')
        if sound.samplerate<100*kHz or sound.samplerate>500*kHz:
            raise ValueError("Samplerate of sound must be between 100 and 500 kHz")
        input = asarray(sound).flatten()
        input = reshape(input, (1, -1))
        allspikes = []
        allvihc = {}
        with in_directory(thisdir):
            with RedirectStdStreams(stdout='null', stderr='null'):
                # optimisation for repeated cfs, only calculate once since there is
                # no random element in this
                cfu = unique(cfs)
                for cf in cfu:
                    vihc = self.catmodel_IHC(input, cf, 1, 
                                    float(1/sound.samplerate),
                                    float(sound.duration)+float(1/sound.samplerate),
                                    cohc, cihc)
                    allvihc[cf] = vihc
                for cf in cfs:
                    vihc = allvihc[cf]
                    synout, psth = self.catmodel_Synapse(vihc, cf, 1,
                                    float(1/sound.samplerate), fibertype, implnt)
                    psth = psth.flatten()
                    spikes, = psth.nonzero()
                    spikes = spikes/sound.samplerate
                    allspikes.append(spikes)
        return allspikes

def cat_an_model(sound, cfs, cohc=1., cihc=1., fibertype=3., implnt=0.):
    if not hasattr(cat_an_matlabmodel, 'prepared'):
        cat_an_matlabmodel.init()
    return cat_an_matlabmodel(sound, cfs, cohc=cohc, cihc=cihc,
                                    fibertype=fibertype, implnt=implnt)

cat_an_matlabmodel = CatANMatlabModel()

defaultparams = Parameters(
    cohc=1.,
    cihc=1.,
    fibertype=3.,
    implnt=0.,
    )

class CatANModel(Model):
    name = 'Cat ANF model from Zilany et al. 2009'
    id = 'cat_an'
    extra_sources = [CatANMatlabModel, cat_an_model]
    @property
    def description(self):
        return '''
        <p>Cat ANF model from Zilany et al. 2009.</p>
        
        <p><b>Parameters</b></p>
        '''+self.parameters_table
    def __init__(self, **params):
        self.params = Parameters(**defaultparams.copy())
        self.params.update(params)
    def start(self):
        set_default_samplerate(100*kHz)
    def run(self, sound, repeats, cfs, params):
        p = Parameters(**self.params.copy())
        p.update(params)
        model = CatANMatlabModel()
        return apply_model(model, sound, repeats, cfs, p)


if __name__=='__main__':
    from dfmgtools.compute import *
    set_default_samplerate(100*kHz)
    if 0:
        import time
        sound = tone(1*kHz, 90*ms).atlevel(40*dB).shifted(10*ms)
        cfs = erbspace(500*Hz, 2*kHz, 200)
        start = time.time()
        i, t = get_spikes_from_trains(cat_an_model(sound, cfs))
        print 'Time', time.time()-start
        plot(t, i, '.k')
    if 1:
        import time
        sound = tone(1*kHz, 90*ms).atlevel(40*dB).shifted(10*ms)
        cfs = erbspace(500*Hz, 2*kHz, 2000)
        cfs[:] = 1*kHz # model is optimised for this case
        model = CatANMatlabModel()
        start = time.time()
        i, t = apply_model(model, sound, len(cfs)/sound.nchannels, cfs, defaultparams)
        print 'Time', time.time()-start
        plot(t, i, '.k')
    show()
    