function wrap(cf, samplerate, cohc, cihc, fibertype, implnt)

id = fopen('tempin.bin', 'r');
pin = fread(id, inf, 'double')';
fclose(id);

%plot(pin)

%cf = 1000;
%samplerate = 100000;
%cohc = 1;
%cihc = 1;
%fibertype = 3;
%implnt = 0;
vihc = catmodel_IHC(pin, cf, 1, 1/samplerate, length(pin)/samplerate, cohc, cihc);
[synout, psth] = catmodel_Synapse(vihc, cf, 1, 1/samplerate, fibertype, implnt);

id = fopen('tempout_vihc.bin', 'w');
fwrite(id, vihc, 'double');
fclose(id);

id = fopen('tempout_synout.bin', 'w');
fwrite(id, synout, 'double');
fclose(id);

id = fopen('tempout_psth.bin', 'w');
fwrite(id, psth, 'double');
fclose(id);

%subplot(411)
%plot(pin)
%subplot(412)
%plot(vihc)
%subplot(413)
%plot(synout)
%subplot(414)
%plot(psth)

