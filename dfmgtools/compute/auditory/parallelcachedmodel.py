'''
TODO: document this.
'''

from brian import *
from brian.hears import *
from brian.utils.progressreporting import ProgressReporter
from dfmgtools.core import *
import os
import multiprocessing
import time
from collections import defaultdict
from hashlib import sha1
from inspect import getsource
import shelve

__all__ = ['apply_model']

def apply_model(model,
                sound, repeats, cfs, params,
                global_repnum=0,
                numcpu=-2, usemp=True,
                debug=False):
    maxblocksize = model.maxblocksize
    multichannel = model.multichannel
    # workaround for the fact that mlabwrap doesn't seem to work with
    # multiple processes on Windows
    if os.name=='nt' and hasattr(model, 'uses_matlab') and model.uses_matlab:
        usemp = False
    if hasattr(model, 'uses_matlab') and model.uses_matlab:
        usemp = False
    # sanity check on data
    if sound.nchannels*repeats!=len(cfs):
        raise ValueError('sound.channels*repeats!=len(cfs)')
    if not isinstance(cfs, ndarray):
        cfs = array(cfs)
    # compute a unique key corresponding to that sound, which will be used to
    # index into a shelf later
    channels = [sound.channel(i) for i in xrange(sound.nchannels)]
    keychannels = [unique_array_key(chan) for chan in channels]
    if debug: print 'keychannels', keychannels
    # compute repnums, anything with the same sound and cf (and params) will
    # be an identical computation, but if we have received a request for
    # multiple such then we introduce an auxiliary repnum parameters that
    # increases for each repetition, this will be combined with the
    # global_repnum applied to the whole block
    items = defaultdict(list)
    channelindex = repeat(arange(sound.nchannels), repeats)
    for i, ch_idx, cf in zip(arange(len(cfs)), channelindex, cfs):
        items[keychannels[ch_idx], cf].append(i)
    repnums = zeros(len(cfs), dtype=int)
    for (ch_key, cf), cf_inds in items.iteritems():
        if debug: print 'item', ch_key, cf, cf_inds
        repnums[cf_inds] = arange(len(cf_inds))
    if debug: print 'repnums', repnums
    # generate string keys for all arguments, the function and sound are handled
    # by a choice of directory and filename, so we only need the rest
    argkeys = [str((repnum, repr(cf))) for repnum, cf in zip(repnums, cfs)]
    if debug: print 'argkeys', argkeys
    # generate filenames
    basepath = os.path.join(cache_path, 'parallelcachedmodel')
    funcpathname = model.__class__.__name__+'_'+sha1(getsource(model.__class__)).hexdigest()
    funcpath = os.path.join(basepath, funcpathname)
    if debug: print 'funcpath', funcpath
    if not os.path.exists(funcpath):
        funcsrcfilename = os.path.join(funcpath, 'func_code.py')
        ensure_directory_of_file(funcsrcfilename)
        open(funcsrcfilename, 'w').write(getsource(model.__class__))
    parampathname = str(hash(repr(params)))
    parampath = os.path.join(funcpath, parampathname)
    if debug: print 'parampath', parampath
    channelfilenames = [os.path.join(parampath, key) for key in keychannels]
    if debug:
        for f in channelfilenames:
            print 'channelpath', f
    # work out what needs computing, and load what has already been cached
    shelves = []
    to_compute = []
    results = {}
    foundshelves = {}
    for ch_idx, ch_key, fname in zip(arange(len(channels)), keychannels,
                                     channelfilenames):
        ensure_directory_of_file(fname)
        if fname in foundshelves:
            shelf = foundshelves[fname]
        else:
            shelf = shelve.open(fname)
            foundshelves[fname] = shelf
        shelves.append(shelf)
        for i in xrange(ch_idx*repeats, (ch_idx+1)*repeats):
            key = argkeys[i]
            if key in shelf:
                results[i] = shelf[key]
            else:
                to_compute.append(i)
    # assign work to blocks
    if debug: print 'to_compute', to_compute
    if len(to_compute):
        blocks = []
        curblock = []
        cur_ch_idx = channelindex[to_compute[0]]
        for i in to_compute:
            ch_idx = channelindex[i]
            endblock = False
            if cur_ch_idx!=ch_idx and not multichannel:
                endblock = True
                cur_ch_idx = ch_idx
            if len(curblock)>=maxblocksize:
                endblock = True
            if endblock:
                blocks.append(array(curblock))
                curblock = []
            curblock.append(i)
        blocks.append(array(curblock))
        # generate list of arguments to send to pool
        args = []
        for block in blocks:
            blockcf = cfs[block]
            blockch_idx = channelindex[block]
            if multichannel:
                blocksound = sound[:, blockch_idx[0]:blockch_idx[-1]+1]
                arg = (blocksound, blockch_idx-blockch_idx[0], blockcf)
            else:
                blocksound = channels[blockch_idx[0]]
                arg = (blocksound, blockcf)
            args.append((arg, params))
            if debug: print 'block', block, cfs[block], channelindex[block]
            if debug: print 'arg', arg[0].shape, arg[1:]
        # send arguments to pool
        if usemp:
            if numcpu<=0:
                numcpu = multiprocessing.cpu_count()+numcpu
            if numcpu<=0:
                numcpu = 1
            if numcpu>len(args):
                numcpu = len(args)
            pool = multiprocessing.Pool(numcpu, pool_init, (model,))
            pool_results = pool.imap(pool_func, args)
        else:
            # only for debugging purposes
            pool_init(model)
            reporter = ProgressReporter('stderr', period=60, first_report=60)
            reporter.start()
            pool_results = []
            for j, arg in enumerate(args):
                pool_results.append(pool_func(arg))
                reporter.update(float(j+1)/len(args))
            reporter.finish()
        # retrieve arguments from pool
        reporter = ProgressReporter('stderr', period=60, first_report=60)
        reporter.start()
        for j, result in enumerate(pool_results):
            if debug: print 'result', j#, result
            block = blocks[j]
            if debug: print 'block', block
            for i, st in zip(block, result):
                results[i] = st
                key = argkeys[i]
                shelf = shelves[channelindex[i]]
                shelf[key] = st
            reporter.update(float(j+1)/len(args))
        if usemp:
            pool.terminate()
        reporter.finish()
    # close all shelves in case they have any unwritten data
    for shelf in shelves:
        shelf.close()
    # reduce computed results
    allj = []
    allt = []
    for i in xrange(len(results)):
        t = results[i]
        j = i*ones(len(t), dtype=int)
        allj.append(j)
        allt.append(t)
    allj = hstack(allj)
    allt = hstack(allt)
    return allj, allt

global_pool_model = None

def pool_init(model):
    global global_pool_model
    global_pool_model = model
    if not hasattr(model, 'prepared'):
        model.init()

def pool_func((args, kwds)):
    return global_pool_model(*args, **kwds)

@func_cache
def unique_array_key(data):
    '''
    Returns a unique short hash key for the array data. First it tries the
    hash, and if this has already been used, it tries hash_1, hash_2, etc.
    The way it works is that @func_cache on unique_array_key already checks if
    the exact array has been called and will return the cached value in that
    case. Otherwise, we repeatedly try possible hashes which are stored by
    @func_cache applied to unique_hash_key (which just returns its argument).
    If it has already been stored then we need to try a new one.
    '''
    hash = sha1(data.view(uint8)).hexdigest()
    i = 1
    while True:
        tryhash = hash+'_'+str(i)
        if not is_cached(unique_hash_key, tryhash):
            return unique_hash_key(tryhash)
        i += 1

@func_cache
def unique_hash_key(hash):
    return hash
