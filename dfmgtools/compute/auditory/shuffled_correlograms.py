from brian import *
from brian.hears import *
from scipy import weave

__all__ = ['SAC', 'SCC_approximate', 'SCC',
           'SAC0', 'SCC0_approximate', 'SCC0',
           ]

# shuffled correlogram helper function
def SC(i, t, f, maxisi, binwidth, duration, repeats, offset=0.0,
       t2=None, i2=None):
    I = argsort(t)
    t = t[I]
    i = i[I]
    N1 = repeats
    if t2 is None:
        t2 = t+offset/f # shift t by half a period in case of SCC
        i2 = i
        N2 = repeats-1
    else:
        I = argsort(t2)
        t2 = t2[I]
        i2 = i2[I]+amax(i)+1
        N2 = repeats
    isi = []
    W = float(maxisi+binwidth)
    n = int(maxisi/binwidth)
    binmids = arange(-n, n+2)*binwidth
    bins = binmids-binwidth/2
    binmids = binmids[:-1]
    h = zeros(len(binmids))
    numbins = len(h)
    numspikes = len(t)
    numspikes2 = len(t2)
    code = '''
    int start=0;
    double isimin = -n*binwidth-binwidth/2.0;
    for(int k=0; k<numspikes; k++)
    {
        double curt = t[k];
        while(start<numspikes && t2[start]<curt-W) start++;
        for(int l=start; l<numspikes2; l++)
        {
            double curt2 = t2[l];
            if(curt2>curt+W) break;
            if(i[k]==i2[l]) continue;
            double isi = curt-curt2;
            if(isi<isimin) continue;
            int bin = (int)((isi-isimin)/binwidth);
            if(bin<0||bin>=numbins) continue;
            h[bin] += 1.0;
        }
    }
    '''
    ns = {'n':n, 'binwidth':float(binwidth), 'h':h, 'numbins':numbins,
          'numspikes':numspikes, 'numspikes2':numspikes2,
          'i':i, 't':t, 'i2':i2, 't2':t2, 'W':W}
    args = ns.keys()
    weave.inline(code, args, ns, compiler='gcc',
                 extra_compile_args=['-O3', '-ffast-math', '-march=native'])
    r1 = len(t)/(duration*repeats)
    r2 = len(t2)/(duration*repeats)
    h = (h*1.0)/(N1*N2*r1*r2*binwidth*duration)
    return binmids, h, h[n]


# peak shuffled correlogram helper function
def SC0(i, t, f, binwidth, duration, repeats, offset=0.0,
        t2=None, i2=None):
    I = argsort(t)
    t = t[I]
    i = i[I]
    N1 = repeats
    if t2 is None:
        t2 = t+offset/f # shift t by half a period in case of SCC
        i2 = i
        N2 = repeats-1
    else:
        I = argsort(t2)
        t2 = t2[I]
        i2 = i2[I]+amax(i)+1
        N2 = repeats
    numspikes = len(t)
    numspikes2 = len(t2)
    code = '''
    double W = 0.5*binwidth;
    int start=0;
    int h = 0;
    for(int k=0; k<numspikes; k++)
    {
        double curt = t[k];
        while(start<numspikes && t2[start]<curt-W) start++;
        for(int l=start; l<numspikes2; l++)
        {
            double curt2 = t2[l];
            if(curt2>curt+W) break;
            if(i[k]==i2[l]) continue;
            double isi = curt-curt2;
            if(isi<-W || isi>W) continue;
            h += 1;
        }
    }
    return_val = h;
    '''
    ns = {'binwidth':float(binwidth),
          'numspikes':numspikes, 'numspikes2':numspikes2,
          'i':i, 't':t, 'i2':i2, 't2':t2}
    args = ns.keys()
    h = weave.inline(code, args, ns, compiler='gcc',
                     extra_compile_args=['-O3', '-ffast-math', '-march=native'])
    r1 = len(t)/(duration*repeats)
    r2 = len(t2)/(duration*repeats)
    c = (h*1.0)/(N1*N2*r1*r2*binwidth*duration)
    return c, h


# shuffled autocorrelogram
def SAC(i, t, f, maxisi, binwidth, duration, repeats):
    '''
    Returns a shuffled autocorrelogram
    
    Returns
    -------
    
    binmids : array
        The centres of the bins (in seconds).
    h : array
        The autocorrelogram value for each bin.
    h[0] : float
        The peak value (for binmid=0).
    '''
    return SC(i, t, f, maxisi, binwidth, duration, repeats, 0.0)

def SAC0(i, t, f, binwidth, duration, repeats):
    '''
    As SAC but only returns a single value for the bin around 0 and the raw
    count of spike coincidences.
    '''
    return SC0(i, t, f, binwidth, duration, repeats, 0.0)


# shuffled approximate cross-polarity correlograms
def SCC_approximate(i, t, f, maxisi, binwidth, duration, repeats):
    '''
    Returns an approximate shuffled cross-correlogram (phase shift all spikes by 0.5 cycles)
    
    Returns
    -------
    
    binmids : array
        The centres of the bins (in seconds).
    h : array
        The cross-correlogram value for each bin.
    h[0] : float
        The value at 0.
    '''
    return SC(i, t, f, maxisi, binwidth, duration, repeats, 0.5)

def SCC0_approximate(i, t, f, binwidth, duration, repeats):
    '''
    As SCC_approximate but only returns a single value for the bin around 0 and the raw
    count of spike coincidences.
    '''
    return SC0(i, t, f, binwidth, duration, repeats, 0.5)


def SCC(i1, t1, i2, t2, f, maxisi, binwidth, duration, repeats):
    '''
    Returns an exact shuffled cross-correlogram if the two spike trains are in response to the
    sound and the polarity-reversed sound.
    
    Returns
    -------
    
    binmids : array
        The centres of the bins (in seconds).
    h : array
        The cross-correlogram value for each bin.
    h[0] : float
        The value at 0.
    '''
    return SC(i1, t1, f, maxisi, binwidth, duration, repeats,
              t2=t2, i2=i2)

def SCC0(i1, t1, i2, t2, f, binwidth, duration, repeats):
    '''
    As SCC but only returns a single value for the bin around 0 and the raw
    count of spike coincidences.
    '''
    return SC0(i1, t1, f, binwidth, duration, repeats,
               i2=i2, t2=t2)
