from hashing import *

__all__ = ['Test']

class Test(NameIdDescriptionSourceHashable):
    name = 'No test'
    id = 'notest'
    description = 'No test'
    def run(self, model):
        return {}
