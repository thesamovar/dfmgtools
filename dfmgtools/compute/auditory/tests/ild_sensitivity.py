from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.test import Test
from dfmgtools.compute import numpy_seeded
from dfmgtools.compute.auditory.filterbanks import GainFilterbank
from standard_tests import *

__all__ = ['ILDSensitivity']

class ILDSensitivity(Test):
    @property
    def name(self):
        return 'ILD sensitivity test ('+self.type+')'
    @property
    def id(self):
        return 'ild_sensitivity_'+self.type
    @property
    def description(self):
        s = '''
        ILD sensitivity test in range [-{self.ildmax}, {self.ildmax}] with
        {self.N} steps at CF {self.cf}, level {self.level}, repeats
        {self.repeats}, duration {self.duration}, ramptime {self.ramptime},
        noise seed {self.noiseseed}.
        '''.format(self=self)
        if self.fixed_max:
            s += 'ILDs are introduced by fixing the higher of the two levels.'
        else:
            s += '''ILDs are introduced by varying the two binaural channels
            equally around a fixed level.'''
        return s
    def __init__(self, ildmax=10*dB, N=50,
                 cf=750*Hz, level=65*dB, repeats=10, duration=100*ms,
                 type='tones', fixed_max=True,
                 ramptime=1*ms,
                 noiseseed=130194320,
                 ):
        self.ildmax = ildmax
        self.N = N
        self.cf = cf
        self.type = type
        self.repeats = repeats
        self.duration = duration
        self.level = level
        self.ramptime = ramptime
        self.noiseseed = noiseseed
        self.fixed_max = fixed_max
        
    def run(self, model):
        if not model.binaural:
            raise ValueError("Can only apply ILD sensitivity test to binaural models")
        
        if self.type=='tones':
            sound = tone(self.cf, self.duration).ramp(duration=self.ramptime)
        elif self.type=='whitenoise':
            with numpy_seeded(self.noiseseed):
                sound = whitenoise(self.duration).ramp(duration=self.ramptime)
        else:
            raise ValueError("Unknown type "+self.type)
        
        # set the level of the sound after passing through a GT filterbank
        # to be equal to the desired level, a rough way to equalise between
        # tones and noises
        lev = sound.level
        newsound = Sound(Gammatone(sound, self.cf).process(), samplerate=sound.samplerate)
        newlev = newsound.level
        sound.level = self.level+lev-newlev
        
        ilds = linspace(-self.ildmax, self.ildmax, self.N)
        if self.fixed_max:
            L = -clip(ilds, 0, Inf)
            R = clip(ilds, -Inf, 0)
            gains = hstack((L, R))
        else:
            gains = hstack((-ilds/2, ilds/2))
        fb = GainFilterbank(sound, gains)
        
        cfs = ones(self.N*self.repeats)*self.cf
        
        i, t = model.run(fb, self.repeats, cfs, {})
        
        j = i
        i = i/self.repeats # now i gives the frequency number
        rates = (bincount(i, minlength=self.N)/self.duration)/self.repeats
        
        figs = {}
        f = figure()
        fakeild = linspace(-self.ildmax, self.ildmax, self.N*self.repeats)
        plot(t, fakeild[j], ',k', ms=2)
        xlabel('Time (s)')
        ylabel('ILD (dB)')
        xlim(0, float(sound.duration))
        ylim(-float(self.ildmax), float(self.ildmax))
        figs['raster'] = ("Raster plot", f)
        f = figure()
        plot(ilds, rates)
        xlabel('ILD (dB)')
        ylabel('Rate (Hz)')
        xlim(-self.ildmax, self.ildmax)
        ylim(0, 400)
        figs['rateild'] = ("Estimated rates in response to ILD", f)
        return {'figures': figs,
                'data':{'rates': rates, 'ilds': ilds},
                }

standard_binaural_tests.append(ILDSensitivity())
