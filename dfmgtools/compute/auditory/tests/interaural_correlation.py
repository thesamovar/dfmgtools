from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.test import Test
from dfmgtools.compute import numpy_seeded
from dfmgtools.compute.auditory.gramschmidt import *
from standard_tests import *

__all__ = ['InterauralCorrelation']

class InterauralCorrelation(Test):
    name = 'Interaural correlation test'
    id = 'iac'
    @property
    def description(self):
        return '''
        Interaural correlation test. N={self.N}, cfs={self.cfs}, levels={self.levels},
        repeats={self.repeats}, duration={self.duration}, ramptime={self.ramptime},
        noiseseed={self.noiseseed}.
        '''.format(self=self)
    def __init__(self, N=20,
                 cfs=[250*Hz, 750*Hz, 3*kHz],
                 levels=[65*dB],
                 repeats=20, duration=500*ms,
                 ramptime=1*ms,
                 noiseseed=130194320,
                 ):
        self.N = N
        self.cfs = cfs
        self.repeats = repeats
        self.duration = duration
        self.levels = levels
        self.ramptime = ramptime
        self.noiseseed = noiseseed
        
    def run(self, model):
        if not model.binaural:
            raise ValueError("Can only apply interaural correlation test to binaural models")
        
        with numpy_seeded(self.noiseseed):
            sound1 = whitenoise(self.duration).ramp(duration=self.ramptime)
            sound2 = whitenoise(self.duration).ramp(duration=self.ramptime)
        
        # make sounds with the given IACs
        R = linspace(-1, 1, self.N)
        left = []
        right = []
        for r in R:
            S = make_correlated(sound1, sound2, r)
            left.append(S.left)
            right.append(S.right)
        sound = Sound(left+right)
        
        # Compute rate curves
        spikes = {}
        rates = {}
        for level in self.levels:
            if not isinstance(level, dB_type):
                level = level*dB
            sound.level = level
            for cf in self.cfs:
                i, t = model.run(sound, self.repeats, ones(self.N*self.repeats)*cf, {})
                spikes[level, cf] = (i, t)
                rate = bincount(i/self.repeats, minlength=self.N)/(self.repeats*self.duration)
                rates[level, cf] = rate

        # Plot figures
        figs = {}
        fig = figure()
        nlevels = max(2, len(self.levels))
        ncfs = max(2, len(self.cfs))
        for ilevel, level in enumerate(self.levels):
            for icf, cf in enumerate(self.cfs):
                plot(R, rates[level, cf], color=(ilevel*1.0/(nlevels-1), 0,
                                                 icf*1.0/(ncfs-1),),
                     label='%s %s'%(level, cf))
        if len(self.levels)*len(self.cfs)<=4:
            legend()
        xlabel("Interaural correlation")
        ylabel("Rate (Hz)")
        xlim(-1, 1)
        ylim(0, 400)
        figs['iac'] = ("Rate with interaural correlation", fig)

        return {'figures':figs,
                'data': {'spikes': spikes,
                         'rates': rates,
                         'R': R,
                         },
                }
        

standard_binaural_tests.append(InterauralCorrelation())


if __name__=='__main__':
    from icdynamic.models.simplemso import *
    model = SimpleMSO(0.0).quickcached()
    model.start()
    test = InterauralCorrelation()
    test.run(model)
    show()
    