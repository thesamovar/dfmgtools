from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.test import Test
from dfmgtools.compute import numpy_seeded
from standard_tests import *

__all__ = ['ITDSensitivity']

class ITDSensitivity(Test):
    @property
    def name(self):
        return 'ITD sensitivity test ('+self.type+')'
    @property
    def id(self):
        return 'itd_sensitivity_'+self.type
    @property
    def description(self):
        return '''
        ITD sensitivity test in range [-{self.itdmax}, {self.itdmax}] with
        {self.N} steps at CF {self.cf}, level {self.level}, repeats
        {self.repeats}, duration {self.duration}, ramptime {self.ramptime},
        noise seed {self.noiseseed}.
        '''.format(self=self)
    def __init__(self, itdmax=1*ms, N=50,
                 cf=750*Hz, level=65*dB, repeats=10, duration=100*ms,
                 type='tones',
                 ramptime=1*ms,
                 noiseseed=130194320,
                 ):
        self.itdmax = itdmax
        self.N = N
        self.cf = cf
        self.type = type
        self.repeats = repeats
        self.duration = duration
        self.level = level
        self.ramptime = ramptime
        self.noiseseed = noiseseed
        
    def run(self, model):
        if not model.binaural:
            raise ValueError("Can only apply ITD sensitivity test to binaural models")
        
        if self.type=='tones':
            sound = tone(self.cf, self.duration).ramp(duration=self.ramptime)
        elif self.type=='whitenoise':
            with numpy_seeded(self.noiseseed):
                sound = whitenoise(self.duration).ramp(duration=self.ramptime)
        else:
            raise ValueError("Unknown type "+self.type)
        
        # set the level of the sound after passing through a GT filterbank
        # to be equal to the desired level, a rough way to equalise between
        # tones and noises
        lev = sound.level
        newsound = Sound(Gammatone(sound, self.cf).process(), samplerate=sound.samplerate)
        newlev = newsound.level
        sound.level = self.level+lev-newlev
        
        itds = linspace(-self.itdmax, self.itdmax, self.N)
        delays = hstack((-itds/2, itds/2))
        fb = FractionalDelay(sound, delays)
        
        cfs = ones(self.N*self.repeats)*self.cf
        
        i, t = model.run(fb, self.repeats, cfs, {})
        
        j = i
        i = i/self.repeats # now i gives the frequency number
        rates = (bincount(i, minlength=self.N)/self.duration)/self.repeats
        
        figs = {}
        f = figure()
        fakeitd = linspace(-self.itdmax, self.itdmax, self.N*self.repeats)
        plot(t, fakeitd[j]/usecond, ',k', ms=2)
        xlabel('Time (s)')
        ylabel('ITD (us)')
        xlim(0, float(sound.duration))
        ylim(-self.itdmax/usecond, self.itdmax/usecond)
        figs['raster'] = ("Raster plot", f)
        f = figure()
        plot(itds/usecond, rates)
        xlabel('ITD (usecond)')
        ylabel('Rate (Hz)')
        xlim(-self.itdmax/usecond, self.itdmax/usecond)
        ylim(0, 400)
        figs['rateitd'] = ("Estimated rates in response to ITD", f)
        return {'figures': figs,
                'data':{'rates': rates, 'itds': itds},
                }

standard_binaural_tests.append(ITDSensitivity())
