from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.test import Test
from dfmgtools.compute import numpy_seeded
from dfmgtools.compute.spiketrains import *
from mpl_toolkits.axes_grid1 import AxesGrid
from standard_tests import *

__all__ = ['ModulationTransferFunction']

class ModulationTransferFunction(Test):
    @property
    def name(self):
        return 'MTF (modulation transfer function) test'
    @property
    def id(self):
        return 'mtf'
    @property
    def description(self):
        return '''
        Modulation transfer function with modulation rates from
        {self.low} to {self.high} in {self.N} log2-spaced steps, carrier
        frequency (and centre frequency) {self.carrier}, level {self.level},
        repeats {self.repeats}, duration {self.duration}, minspikes
        {self.minspikes}.
        '''.format(self=self)
    def __init__(self, low=1*Hz, high=128*Hz, N=8,
                 carrier=5*kHz, level=65*dB, repeats=10, duration=1000*ms,
                 minspikes=50,
                 ):
        self.low = low
        self.high = high
        self.N = N
        self.cf = self.carrier = carrier
        self.repeats = repeats
        self.duration = duration
        self.level = level
        self.minspikes = minspikes
        
    def run(self, model):
        
        modfreq = logspace(log2(self.low), log2(self.high), self.N, base=2)
        sound = tone(ones(self.N)*self.carrier, self.duration)
        sound *= (1+sin(-pi/2+2*pi*modfreq*sound.times[:, newaxis]))/2
        
        sound.level = self.level
        
        cfs = ones(self.N*self.repeats)*self.cf
        
        i, t = model.run(sound, self.repeats, cfs, {})
        
        j = i
        i = i/self.repeats # now i gives the frequency number
        # compute rates
        rates = (bincount(i, minlength=self.N)/self.duration)/self.repeats
        # compute VS_modfreq
        trains = get_trains_from_spikes(i, t, imax=self.N)
        VS = []
        allhist = []
        for fm, train in zip(modfreq, trains):
            theta = 2*pi*fm*train
            allhist.append(theta%(2*pi))
            if len(theta)>=self.minspikes:
                vs = abs(sum(exp(1j*theta))/len(theta))
            else:
                vs = 0.0
            VS.append(vs)
        VS = array(VS)
        
        figs = {}
        f = figure()
        fakemodfreq = logspace(log2(self.low), log2(self.high),
                               self.N*self.repeats, base=2)
        semilogy(t, fakemodfreq[j], ',k', basey=2)
        xlabel('Time (s)')
        ylabel('Modulation frequency (Hz)')
        xlim(0, float(sound.duration))
        ylim(self.low, self.high)
        figs['raster'] = ("Raster plot", f)

        # RMTF figure
        f = figure()
        semilogx(modfreq, rates, basex=2)
        xlabel('Modulation frequency (Hz)')
        ylabel('Rate (Hz)')
        xlim(self.low, self.high)
        ylim(0, 400)
        figs['rmtf'] = ("Estimated rates in response to AM sounds with different modulation frequencies", f)
        
        # TMTF figure
        f = figure()
        semilogx(modfreq, VS, basex=2)
        xlabel('Modulation frequency (Hz)')
        ylabel('Vector strength wrt mod. freq. (Hz)')
        xlim(self.low, self.high)
        ylim(0, 1)
        figs['tmtf'] = ("Modulation vector strength in response to AM sounds with different modulation frequencies", f)
        
        # TMTF histogram figure
        f = figure()
        g = AxesGrid(f, 111, (len(modfreq), 1), aspect=False, axes_pad=0.0, share_all=True)
        for k, h in enumerate(allhist):
            if len(h):
                c = rand(3)
                c = (c-amin(c))/(amax(c)-amin(c))
                g[k].hist(h, histtype='bar', facecolor=tuple(c))
                g[k].set_ylabel('%d Hz'%int(modfreq[k]))
        for gax in g.axes_all:
            gax.set_yticks([])
            gax.set_xlim(0, 2*pi)
        figs['tmtf_hist'] = ("Spike modulation period histograms", f)        
        
        return {'figures': figs,
                'data':{'rates': rates, 'modfreq': modfreq, 'VS':VS},
                }

standard_monaural_tests.append(ModulationTransferFunction())
standard_binaural_tests.append(ModulationTransferFunction())
