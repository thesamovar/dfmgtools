from brian import *
from brian.hears import *
from ..test import Test
from standard_tests import *

__all__ = ['RateFrequency']

class RateFrequency(Test):
    @property
    def name(self):
        return 'Rate/frequency test ('+self.type+')'
    @property
    def id(self):
        return 'ratefreq'+self.type
    @property
    def description(self):
        return '''
        Tests the rate as a function of frequency for {self.type} at CF between
        {self.low} and {self.high} ({self.N} ERB-spaced), with {self.repeats}
        repeats, duration {self.duration}, ramptime {self.ramptime}, level
        {self.level}. (Noise seed is {self.noiseseed}.)
        '''.format(self=self)
    def __init__(self, low=100*Hz, high=20*kHz, N=20, level=60*dB, repeats=50, duration=100*ms,
                 type='tones',
                 ramptime=2*ms,
                 binwidth=.1*ms,
                 isi_max=10*ms,
                 noiseseed=130194320):
        self.type = type
        self.low = low
        self.high = high
        self.N = N
        self.tones = [f*Hz for f in erbspace(low, high, N)]
        self.repeats = repeats
        self.duration = duration
        self.level = level
        self.binwidth = binwidth
        self.isi_max = isi_max
        self.ramptime = ramptime
        self.noiseseed = noiseseed
    def run(self, model):
        figs = {}
        rates = []
        tonefreq = repeat(self.tones, self.repeats)
        seed(self.noiseseed)
        if self.type=='tones':
            sound = tone(array(self.tones),
                         self.duration).ramp(duration=self.ramptime)
            tonereps = self.repeats
        elif self.type=='whitenoise':
            sound = whitenoise(self.duration).ramp(duration=self.ramptime)
            tonereps = len(self.tones)*self.repeats
        elif self.type=='pinknoise':
            sound = pinknoise(self.duration).ramp(duration=self.ramptime)
            tonereps = len(self.tones)*self.repeats
        elif self.type=='localpinknoise':
            sound = pinknoise(self.duration, nchannels=len(self.tones))
            localsound = Sound(Gammatone(sound, array(self.tones)).process(),
                               sound.samplerate)
            attenuation = localsound.level-sound.level
            sound.level = array(self.level)-attenuation
            sound = sound.ramp(duration=self.ramptime)
            tonereps = self.repeats
        elif self.type=='bandpassnoise':
            sound = whitenoise(self.duration).ramp(duration=self.ramptime)
            sound = Sound(Gammatone(sound, array(self.tones)).process(),
                          sound.samplerate)
            tonereps = self.repeats
        if self.type!='localpinknoise':
            sound.level = self.level
        cf = tonefreq
        i, t = model.run(sound, tonereps, cf, {})
        j = i
        i = i/self.repeats # now i gives the frequency number
        if len(i):
            rates = (bincount(i)/self.duration)/self.repeats
        else:
            rates = array([])
        if len(rates)<len(self.tones):
            rates = hstack((rates, zeros(len(self.tones)-len(rates))))
        f = figure()
        jnorm = array(j, dtype=float)/self.repeats
        plot(t, jnorm, '.k', ms=2)
        y, _ = yticks()
        newy = array(y, dtype=int)
        newy = newy[newy>=0]
        newy = newy[newy<len(self.tones)]
        newylabels = map(str, array(self.tones, dtype=int)[newy])
        yticks(newy, newylabels)
        xlabel('Time (s)')
        ylabel('Frequency (Hz)')
        figs['raster'] = ("Raster plot", f)
        f = figure()
        semilogx(self.tones, rates)
        xlabel("Frequency (Hz)")
        ylabel("Firing rate (Hz)")
        figs['ratefreq'] = ("Estimated rates at level "+str(self.level), f)
        return {'figures':figs,
                'data':{'rates':rates, 'freqs':self.tones},
                }

standard_monaural_tests.append(RateFrequency())
standard_binaural_tests.append(RateFrequency())
