from brian import *
from brian.hears import *
from ..test import Test
from standard_tests import *

__all__ = ['RateLevel']

class RateLevel(Test):
    name = 'Rate/level test'
    id = 'ratelevel'
    @property
    def description(self):
        return '''
        Tests the adapted rate as a function of level for {self.N} levels between
        {self.low} and {self.high}, using a tone at frequencies {self.freqs},
        with {self.repeats} repeats, duration {self.duration} and ramptime
        {self.ramptime}. Rates are computed using only the second half of the
        signal, and the graph shows a range of rates from 0 Hz to {self.maxrate}.
        '''.format(self=self)
    def __init__(self, low=0*dB, high=90*dB, N=40,
                 freqs=[500*Hz, 1*kHz, 2*kHz, 4*kHz, 8*kHz], repeats=50, duration=100*ms,
                 binwidth=.1*ms,
                 ramptime=2*ms,
                 isi_max=10*ms, maxrate=400*Hz):
        self.low = low
        self.high = high
        self.N = N
        self.levels = [l*dB for l in linspace(low, high, N)]
        self.repeats = repeats
        self.duration = duration
        self.freqs = freqs
        self.binwidth = binwidth
        self.isi_max = isi_max
        self.ramptime = ramptime
        self.maxrate = maxrate
    def run(self, model):
        figs = {}
        rates = []
        sound = tone(repeat(self.freqs, self.N), self.duration,
                     ).ramp(duration=self.ramptime)
        sound.level = tile(self.levels, len(self.freqs))
        cf = repeat(self.freqs, self.N*self.repeats)
        i, t = model.run(sound, self.repeats, cf, {})
        j = i
        i = i/self.repeats # now i gives the level+freq number
        i = i[t>=float(self.duration)/2]
        rates = (bincount(i)/(self.duration/2))/self.repeats
        f = figure()
        plot(t, j, '.k', ms=1)
        xlabel('Time (s)')
        ylabel('Neuron index')
        figs['raster'] = ("Raster plot", f)
        f = figure()
        for k, freq in enumerate(self.freqs):
            plot(self.levels,
                 rates[k*len(self.levels):(k+1)*len(self.levels)],
                 label=str(freq))
        xlabel("Level (dB)")
        ylabel("Firing rate (Hz)")
        ylim(0, float(self.maxrate))
        legend(loc='upper left')
        figs['ratelevel'] = ("Estimated rates", f)
        return {'figures':figs,
                }

standard_monaural_tests.append(RateLevel())
standard_binaural_tests.append(RateLevel())
