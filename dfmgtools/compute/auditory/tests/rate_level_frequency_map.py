from brian import *
from brian.hears import *
from ..test import Test
from dfmgtools.results.auditory import *
from standard_tests import *

__all__ = ['RateLevelFrequencyMap']

class RateLevelFrequencyMap(Test):
    name = 'Rate map as a function of level and frequency'
    id = 'ratelevelfrequencymap'
    @property
    def description(self):
        return '''
        Tests the adapted rate as a function of level for {self.level_N} levels
        between {self.level_low} and {self.level_high}, using a tone at
        {self.freq_N} frequencies log spaced between {self.freq_low} and
        {self.freq_high},
        with {self.repeats} repeats, duration {self.duration} and ramptime
        {self.ramptime}. Rates are computed using only the second half of the
        signal, and the colour scale shows a range of rates from 0 Hz to
        {self.maxrate}.
        '''.format(self=self)
    def __init__(self,
                 level_low=0*dB, level_high=90*dB, level_N=25,
                 freq_low=100*Hz, freq_high=10*kHz, freq_N=25,
                 repeats=20, duration=100*ms,
                 ramptime=2*ms, maxrate=400*Hz):
        self.level_low = level_low
        self.level_high = level_high
        self.level_N = level_N
        self.levels = [l*dB for l in linspace(level_low, level_high, level_N)]
        self.freq_low = freq_low
        self.freq_high = freq_high
        self.freq_N = freq_N
        self.freqs = logspace(log10(float(freq_low)),
                              log10(float(freq_high)),
                              freq_N)
        self.repeats = repeats
        self.duration = duration
        self.ramptime = ramptime
        self.maxrate = maxrate
    def run(self, model):
        figs = {}
        rates = []
        sound = tone(repeat(self.freqs, self.level_N), self.duration,
                     ).ramp(duration=self.ramptime)
        sound.level = tile(self.levels, len(self.freqs))
        cf = repeat(self.freqs, self.level_N*self.repeats)
        i, t = model.run(sound, self.repeats, cf, {})
        j = i
        i = i/self.repeats # now i gives the level+freq number
        i = i[t>=float(self.duration)/2]
        rates = (bincount(i)/(self.duration/2))/self.repeats
        if len(rates)<self.freq_N*self.level_N:
            rates = hstack((rates, zeros(self.freq_N*self.level_N-len(rates))))
        f = figure()
        plot(t, j, '.k', ms=1)
        xlabel('Time (s)')
        ylabel('Neuron index')
        figs['raster'] = ("Raster plot", f)
        f = figure()
        rates.shape = (self.freq_N, self.level_N)
        rates = rates.T
        responsemap(self.freq_low, self.freq_high, self.freq_N,
                    self.level_low, self.level_high, self.level_N,
                    rates, fig=f, ratemax=self.maxrate)
        figs['ratelevel'] = ("Rate map", f)
        return {'figures':figs,
                }

standard_monaural_tests.append(RateLevelFrequencyMap())
standard_binaural_tests.append(RateLevelFrequencyMap())
