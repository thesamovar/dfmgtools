from brian import *
from brian.hears import *
from dfmgtools.compute.auditory.test import Test
from scipy import weave
from brian.utils.progressreporting import *
from standard_tests import *
from dfmgtools.compute.auditory.shuffled_correlograms import *

__all__ = ['SingleFrequencyCorrelation',
           'MultiFrequencyCorrelation',
           ]

class SingleFrequencyCorrelation(Test):
    @property
    def name(self):
        return 'Single frequency correlation test (%s)' % self.type
    @property
    def id(self):
        return 'sf_correlation_'+self.type
    @property
    def description(self):
        return '''
        Single frequency correlation analysis.
        type={self.type},
        modfreq={self.modfreq},
        cf={self.cf},
        filename={self.filename},
        (levelmin, levelmax, levelN) = ({self.levelmin}, {self.levelmax}, {self.levelN}),
        duration={self.duration},
        repeats={self.repeats},
        maxisi={self.maxisi},
        displaymaxisi={self.displaymaxisi},
        binwidth={self.binwidth}.
        '''.format(self=self)
    def __init__(self,
                 type='sam',
                 modfreq=50*Hz, # for type='sam'
                 cf=2*kHz,
                 filename=None, # for type='file'
                 levelmin=0*dB, levelmax=70*dB, levelN=15,
                 duration=600*ms,
                 repeats=30,
                 maxisi=25*ms, displaymaxisi=3*ms,
                 binwidth=50*usecond,
                 ):
        self.type = type
        self.modfreq = modfreq
        self.cf = cf
        self.filename = filename
        self.levelmin = levelmin
        self.levelmax = levelmax
        self.levelN = levelN
        self.levels = linspace(levelmin, levelmax, levelN)
        self.repeats = repeats
        self.duration = duration
        self.binwidth = binwidth
        self.maxisi = maxisi
        self.displaymaxisi = displaymaxisi
        
    def run(self, model):
        (tau, sac, scc, sumcor, difcor, ratio, spikes,
         R, Rc, sound, allpeaksumcor, _, allsacscc, ibml) = SAC_SCC(
                model, self.type, self.cf, self.modfreq, self.levels,
                self.maxisi, self.binwidth, self.duration, self.repeats,
                filename=self.filename)
        i, t = spikes
        
        #### Plot figures
        figs = {}
        # Raster plot
        fig = figure()
        fakelevels = linspace(amin(self.levels), amax(self.levels), self.repeats*len(self.levels))
        plot(t, fakelevels[i], ',k', ms=2)
        xlabel('Time (s)')
        ylabel('Level (dB)')
        fdur = float(sound.duration)
        xlim(0, fdur)
        ylim(amin(self.levels), amax(self.levels))
        lbml = fakelevels[ibml*self.repeats]
        lbml2 = fakelevels[(ibml+1)*self.repeats-1]
        fill([0, fdur, fdur, 0], [lbml, lbml, lbml2, lbml2], fc=(0.7,)*3, ec='none')
        figs['raster'] = ("Raster plot (BML highlighted)", fig)
        
        # Plot synchronisation index with level
        fig = figure()
        plot(self.levels, R, label='Modulation')
        plot(self.levels, Rc, label='Carrier')
        legend()
        xlabel('Level (dB)')
        ylabel('Vector strength')
        ylim(0, 1)
        figs['vs'] = ("Vector strength with level", fig)
        
        # Plot sumcor with level
        fig = figure()
        plot(self.levels[1:-1], allpeaksumcor)
        xlabel('Level (dB)')
        ylabel('Peak SUMCOR')
        ylim(0, 4)
        figs['peaksumcor'] = ("Peak SUMCOR with level", fig)
        
        # Plot SAC and SCC
        fig = figure()
        plot(tau, sac, label='SAC')
        plot(tau, scc, label='SCC')
        legend()
        xlabel('ISI (s)')
        ylabel('SAC/SCC')
        xlim(-self.displaymaxisi, self.displaymaxisi)
        ylim(0, 8)
        figs['sacscc'] = ("SAC and SCC", fig)

        # Plot DIFCOR
        fig = figure()
        plot(tau, difcor)
        xlabel('ISI (s)')
        ylabel('DIFCOR')
        xlim(-self.displaymaxisi, self.displaymaxisi)
        ylim(-8, 8)
        figs['difcor'] = ("DIFCOR", fig)

        # Plot SUMCOR
        fig = figure()
        # not sure why, but +0.25/cf seems to centre it correctly, maybe something to
        # do with FFT/IFFT being not quite right?
        plot(tau+0.25/self.cf, sumcor)
        xlabel('ISI (s)')
        ylabel('SUMCOR')
        xlim(-self.displaymaxisi, self.displaymaxisi)
        ylim(0, 4)
        figs['sumcor'] = ("SUMCOR", fig)
        
        #### Return figures and data
        return {'figures':figs,
                'data': {
                    'tau': tau, 'sac': sac, 'scc': scc, 'sumcor': sumcor,
                    'difcor': difcor, 'ratio': ratio, 'spikes': spikes,
                    'R': R, 'Rc': Rc, 'allpeaksumcor': allpeaksumcor,
                    'allsacscc':allsacscc,
                    'ibml':ibml,
                    },
                }        


class MultiFrequencyCorrelation(Test):
    @property
    def name(self):
        return 'Multiple frequency correlation test (%s)' % self.type
    @property
    def id(self):
        return 'mf_correlation_'+self.type
    @property
    def description(self):
        return '''
        Multiple frequency correlation analysis.
        type={self.type},
        modfreq={self.modfreq},
        (cfmin, cfmax, cfN) = ({self.cfmin}, {self.cfmax}, {self.cfN}),
        filename={self.filename},
        (levelmin, levelmax, levelN) = ({self.levelmin}, {self.levelmax}, {self.levelN}),
        duration={self.duration},
        repeats={self.repeats},
        maxisi={self.maxisi},
        displaymaxisi={self.displaymaxisi},
        binwidth={self.binwidth}.
        '''.format(self=self)
    def __init__(self,
                 type='sam',
                 modfreq=50*Hz, # for type='sam'
                 cfmin=300*Hz, cfmax=10*kHz, cfN=20,
                 filename=None, # for type='file'
                 levelmin=0*dB, levelmax=70*dB, levelN=15,
                 duration=600*ms,
                 repeats=30,
                 maxisi=25*ms, displaymaxisi=3*ms,
                 binwidth=50*usecond,
                 ):
        self.type = type
        self.modfreq = modfreq
        self.cfmin = cfmin
        self.cfmax = cfmax
        self.cfN = cfN
        self.cfs = erbspace(cfmin, cfmax, cfN)
        self.filename = filename
        self.levelmin = levelmin
        self.levelmax = levelmax
        self.levelN = levelN
        self.levels = linspace(levelmin, levelmax, levelN)
        self.repeats = repeats
        self.duration = duration
        self.binwidth = binwidth
        self.maxisi = maxisi
        self.displaymaxisi = displaymaxisi
        
    def run(self, model):
        (ratios, maxdifcorpeaks, maxsumcorpeaks, maxsyncindices,
         sac0, scc0) = multi_SAC_SCC(
                    model, self.type, self.cfs, self.modfreq, self.levels, self.maxisi,
                    self.binwidth, self.duration, self.repeats)
        #### Plot figures
        figs = {}
        # SAC and SCC with frequency
        fig = figure()
        semilogx(self.cfs, sac0, label='SAC(0)')
        semilogx(self.cfs, scc0, label='SCC(0)')
        axis('tight')
        log_frequency_xaxis_labels()
        ylim(0, 8)
        xlabel('Frequency (Hz)')
        ylabel('SAC(0) and SCC(0)')
        figs['sac0scc0'] = ("SAC(0) and SCC(0) with frequency", fig)

        # Max sumcor peak with frequency
        fig = figure()
        semilogx(self.cfs, maxsumcorpeaks)
        axis('tight')
        log_frequency_xaxis_labels()
        xlabel('Frequency (Hz)')
        ylabel('Max SUMCOR peak')
        ylim(1, 5)
        figs['maxsumcorpeak'] = ("Max SUMCOR peak with CF", fig)

        # Max VS with frequency
        fig = figure()
        semilogx(self.cfs, maxsyncindices)
        axis('tight')
        log_frequency_xaxis_labels()
        xlabel('Frequency (Hz)')
        ylabel('Max vector strength')
        ylim(0, 1)
        figs['maxvs'] = ("Max vector strengths with CF", fig)

        # Max DIFCOR peak with frequency
        fig = figure()
        semilogx(self.cfs, maxdifcorpeaks)
        axis('tight')
        log_frequency_xaxis_labels()
        xlabel('Frequency (Hz)')
        ylabel('Max DIFCOR peak')
        ylim(0, 8)
        figs['maxdifcorpeak'] = ("Max DIFCOR peak with CF", fig)

        # SCC/SAC ratio with frequency
        fig = figure()
        semilogx(self.cfs, ratios)
        axis('tight')
        log_frequency_xaxis_labels()
        xlabel('Frequency (Hz)')
        ylabel('SCC/SAC ratio')
        ylim(0, 1)
        figs['sccsacratio'] = ("SCC/SAC ratio with CF", fig)

        #### Return figures and data
        return {'figures':figs,
                'data': {
                   'ratios':ratios,
                   'maxdifcorpeaks':maxdifcorpeaks,
                   'maxsumcorpeaks':maxsumcorpeaks,
                   'maxsyncindices':maxsyncindices,
                   'sac0':sac0, 
                   'scc0':scc0,
                    },
                }        


# synchronisation indices
def sync_indices(modfreq, i, t, repeats, N):
    ilevel = i/repeats
    R = []
    for j in arange(N):
        s = t[ilevel==j]
        r = abs(sum(exp(2*pi*modfreq*s*1j)))/len(s)
        R.append(r)
    return array(R)


def SUMCOR(tau, sac, scc, cf):
    samplerate = 1.0/(tau[1]-tau[0])
    s = 0.5*(sac+scc)
    # now filter out high frequency components
    s_ft = fft(s)#, int(2**ceil(log2(len(s)))))
    f = fftfreq(len(s_ft), 1./samplerate)
    # Heinz & Swaminathan say to remove >=cf, but that still seems to leave some
    # oscillatory component, but cf/2 seems to do the trick
    I = abs(f)>=cf/2
    s_ft[I] = 0
    s = ifft(s_ft)
    return s.real[:len(tau)]


def SAC_SCC(model, type, cf, modfreq, levels, maxisi, binwidth, duration, repeats,
            filename=None):
    #### Stimulus
    if type=='sam':
        sound = tone(ones(len(levels))*cf, duration)
        sound *= (1+sin(-pi/2+2*pi*modfreq*sound.times[:, newaxis]))/2
    elif self.type=='file':
        raise ValueError
    else:
        raise ValueError("Type "+self.type+" not known.")
    sound.level = levels
    # Run model    
    i, t = model.run(sound, repeats, ones(sound.nchannels*repeats)*cf, {})
    spikes = (i, t)
    # Compute synchronisation index
    R = sync_indices(modfreq, i, t, repeats, len(levels))    
    Rc = sync_indices(cf, i, t, repeats, len(levels))
    # use only spike times at BML+-5 dB
    ilevel = i/repeats
    #ibml = argmax(R) # BML
    peaksumcor = -1
    allpeaksumcor = []
    difcorpeaks = []
    allsacscc = []
    for ibml in xrange(1, len(R)-1):
        I, = ((ilevel==ibml-1)+(ilevel==ibml)+(ilevel==ibml+1)).nonzero()
        ct = t[I]
        ci = i[I]
        # Compute SAC/SCC/DIFCOR/SUMCOR
        tau, sac, sac0 = SAC(ci, ct, cf, maxisi, binwidth, duration, repeats)
        tau, scc, scc0 = SCC_approximate(ci, ct, cf, maxisi, binwidth, duration, repeats)
        difcor = sac-scc
        difcor0 = difcor[len(tau)/2]
        sumcor = SUMCOR(tau, sac, scc, cf)
        sumcor0 = sumcor[len(tau)/2]
        allpeaksumcor.append(sumcor0)
        if sumcor0>peaksumcor:
            best = (sac, sac0, scc, scc0, difcor, sumcor, ibml)
            peaksumcor = sumcor0
        difcorpeaks.append(difcor0)
        allsacscc.append((sac, scc))
    maxdifcorpeak = amax(difcorpeaks)
    sac, sac0, scc, scc0, difcor, sumcor, ibml = best 
    ratio = scc0/sac0
    return (tau, sac, scc, sumcor, difcor, ratio, spikes, R, Rc, sound,
            array(allpeaksumcor), maxdifcorpeak, allsacscc, ibml)


def multi_SAC_SCC(model, type, cfs, modfreq, levels, maxisi, binwidth, duration, repeats,
                  filename=None):
    ratios = []
    maxdifcorpeaks = []
    maxsumcorpeaks = []
    maxsyncindices = []
    sac0 = []
    scc0 = []
    progress = ProgressReporter()
    progress.start()
    for i, cf in enumerate(cfs):
        (tau, sac, scc, _, _, ratio, _, sync_indices, _, _, allpeaksumcor,
         maxdifcorpeak, _, _) = SAC_SCC(model, type,
                                     cf, modfreq, levels, maxisi,
                                     binwidth, duration, repeats, filename=filename)
        ratios.append(ratio)
        maxdifcorpeaks.append(maxdifcorpeak)
        maxsumcorpeaks.append(amax(allpeaksumcor))
        maxsyncindices.append(amax(sync_indices))
        sac0.append(sac[len(tau)/2])
        scc0.append(scc[len(tau)/2])
        progress.update(float(i+1)/len(cfs))
    progress.finish()
    return (array(ratios), array(maxdifcorpeaks), array(maxsumcorpeaks),
            array(maxsyncindices), array(sac0), array(scc0))


standard_monaural_tests.append(SingleFrequencyCorrelation())
standard_binaural_tests.append(SingleFrequencyCorrelation())

standard_monaural_tests.append(MultiFrequencyCorrelation())
standard_binaural_tests.append(MultiFrequencyCorrelation())


if __name__=='__main__':
    from dfmgtools.compute.auditory.models.zilanycarneyjasa2009 import *
    model = CatANModel()
    model.start()
    test = SingleFrequencyCorrelation()
    #test = MultiFrequencyCorrelation()
    test.run(model)
    show()
    