from brian import *
from brian.hears import *
from ..test import Test
from standard_tests import *

__all__ = ['Spontaneous']

class Spontaneous(Test):
    name = 'Spontaneous firing test'
    id = 'spontaneous'
    @property
    def description(self):
        return '''
        Tests the spontaneous firing rate and statistics for neurons with CFs
        at {cfs}, each for duration {duration}, repeated {repeats} times.
        '''.format(cfs=self.cfs, duration=self.duration, repeats=self.repeats)
    def __init__(self, cfs=[1*kHz], repeats=50, duration=100*ms):
        self.cfs = cfs
        self.duration = duration
        self.repeats = repeats
    def run(self, model):
        figs = {}
        table = {}
        figs_order = []
        sound = silence(self.duration)
        for cf in self.cfs:
            key = str(int(cf))
            cfs = array([cf]*self.repeats)
            i, t = model.run(sound, self.repeats, cfs, {})
            f = figure()
            ax = f.add_subplot(111)
            ax.plot(t, i, '.k', ms=2)
            figs['raster'+key] = ("Raster plot at CF="+str(cf), f)
            figs_order.append('raster'+key)
            est_rate = (len(i)/self.duration)/self.repeats
            table['Estimated rate at CF='+str(cf)] = est_rate
        return {'figures':figs,
                'figures_order':figs_order,
                'table':table,
                }

standard_monaural_tests.append(Spontaneous())
standard_binaural_tests.append(Spontaneous())
