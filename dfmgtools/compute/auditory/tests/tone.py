from brian import *
from brian.hears import *
from ..test import Test
from dfmgtools.results.auditory.psth import stat_figure
from standard_tests import *

__all__ = ['Tone']

class Tone(Test):
    @property
    def name(self):
        return 'Tone response test ({level})'.format(level=self.level)
    id = 'tone'
    @property
    def description(self):
        return '''
        Tests the response to a set of tones at CF. The tones are
        {tones} at level {level}, and each is repeated {repeats} times, for a tone
        duration of {duration} with a ramp time of {ramptime}.
        '''.format(tones=self.tones, repeats=self.repeats,
                   duration=self.duration, level=self.level,
                   ramptime=self.ramptime)
    def __init__(self, tones, level=60*dB, repeats=250, duration=100*ms,
                 binwidth=.1*ms,
                 isi_max=10*ms,
                 ramptime=2*ms):
        self.tones = tones
        self.repeats = repeats
        self.duration = duration
        self.level = level
        self.binwidth = binwidth
        self.isi_max = isi_max
        self.ramptime = ramptime
    def run(self, model):
        figs = {}
        table = {}
        figs_order = []
        for tonefreq in self.tones:
            key = str(int(tonefreq))
            sound = tone(tonefreq, self.duration).ramp(duration=self.ramptime)
            sound.level = self.level
            cf = [tonefreq]*self.repeats
            i, t = model.run(sound, self.repeats, cf, {})
            # Stats figure
            f = figure(figsize=(12, 9))
            stat_figure(i, t, histwidth=self.binwidth, isi_max=self.isi_max)
            figs['stat'+key] = ("Statistics for tone=CF="+str(tonefreq), f)
            figs_order.append('stat'+key) 
            # table of estimated parameters
            est_rate = (len(i)/self.duration)/self.repeats
            table['Estimated rate for tone at '+str(tonefreq)] = est_rate            
        return {'figures':figs,
                'figures_order':figs_order,
                'table':table,
                }

standard_monaural_tests.append(Tone([100*Hz, 500*Hz, 5*kHz]))
standard_binaural_tests.append(Tone([100*Hz, 500*Hz, 5*kHz]))
