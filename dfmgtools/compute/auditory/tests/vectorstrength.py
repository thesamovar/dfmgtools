from brian import *
from brian.hears import *
from ..test import Test
from collections import defaultdict
from standard_tests import *

__all__ = ['VectorStrength']

class VectorStrength(Test):
    name = 'Vector strength test'
    id = 'vectorstrength'
    @property
    def description(self):
        return '''
        Tests the vector strength for {self.N} frequencies ERB-spaced in the
        range [{self.low}, {self.high}], using tones at CF, at levels
        {self.levels} each repeated {self.repeats} times, of duration
        {self.duration} and ramptime {self.ramptime} (spikes during the
        ramp are omitted). VS is set to 0 unless there are at least
        {self.minspikes} spikes to compute the statistic from.
        '''.format(self=self)
    def __init__(self, low=100*Hz, high=5*kHz, N=20,
                 levels=[15*dB, 30*dB, 45*dB, 60*dB, 75*dB, 90*dB],
                 repeats=50, duration=150*ms,
                 binwidth=.1*ms, ramptime=2*ms, isi_max=10*ms,
                 minspikes=5,
                 ):
        self.low = low
        self.high = high
        self.N = N
        self.tones = [f*Hz for f in erbspace(low, high, N)]
        self.repeats = repeats
        self.duration = duration
        self.levels = levels
        self.binwidth = binwidth
        self.isi_max = isi_max
        self.ramptime = ramptime
        self.minspikes = minspikes
    def run(self, model):
        figs = {}
        rates = []
        tonefreq = repeat(self.tones, len(self.levels))
        sound = tone(tonefreq, self.duration).ramp(duration=self.ramptime)
        sound.level = tile(self.levels, len(self.tones))
        cf = repeat(self.tones, len(self.levels)*self.repeats)
        i, t = model.run(sound, self.repeats, cf, {})
        j = i
        s = t
        I = logical_and(t>2*self.ramptime, t<sound.duration-self.ramptime)
        i = i[I]
        t = t[I]
        freq_inds = i/(self.repeats*len(self.levels))
        level_inds = (i%(self.repeats*len(self.levels)))/self.repeats
        repeat_inds = i%self.repeats
        all_vs = []
        repvs = {}
        for freq_idx, freq in enumerate(self.tones):
            curvs = []
            for level_idx, level in enumerate(self.levels):
                I = logical_and(freq_inds==freq_idx, level_inds==level_idx)
                theta = 2*pi*freq*t[I]
                if len(theta)>=self.minspikes:
                    vs = abs(sum(exp(1j*theta))/len(theta))
                else:
                    vs = 0.0
                curvs.append(vs)
                repvs[freq_idx, level_idx] = vs
            curvs = array(curvs)
            fcurvs = curvs[-isnan(curvs)]
            if len(fcurvs):
                maxvs = amax(fcurvs)
            else:
                maxvs = nan
            all_vs.append(maxvs)
        f = figure()
        plot(s, j, '.k', ms=2)
        xlabel('Time (s)')
        ylabel('Neuron index')
        figs['raster'] = ("Raster plot", f)
        f = figure()
        semilogx(self.tones, all_vs)
        xlabel("Frequency (Hz)")
        ylabel("Vector strength")
        ylim(0, 1)
        figs['vs'] = ("Vector strength", f)
        f = figure()
        for level_idx, level in enumerate(self.levels):
            vs = [repvs[freq_idx, level_idx] for freq_idx in xrange(len(self.tones))]
            semilogx(self.tones, vs, label=str(level))
        semilogx(self.tones, all_vs, '-k', label='max', lw=2)
        legend(loc='upper right')
        xlabel("Frequency (Hz)")
        ylabel("Vector strength")
        ylim(0, 1)
        figs['vslev'] = ("Vector strength at different levels", f)
        return {'figures':figs,
                }

standard_monaural_tests.append(VectorStrength())
standard_binaural_tests.append(VectorStrength())
