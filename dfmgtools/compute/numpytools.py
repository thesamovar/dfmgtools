'''
Various add-ons to numpy.
'''

import numpy, numpy.random

__all__ = ['numpy_seeded',
           ]


class numpy_seeded(object):
    '''
    Work with numpy temporarily given a specific seed, the random number
    state is replaced at the end of the block.
    '''
    def __init__(self, seed):
        self.seed = seed
    def __enter__(self):
        self.state = numpy.random.get_state()
        numpy.random.seed(self.seed)
    def __exit__(self,*exc_info):
        numpy.random.set_state(self.state)


if __name__=='__main__':
    print numpy.random.rand()
    with numpy_seeded(21343):
        print numpy.random.rand()
    print numpy.random.rand()
    