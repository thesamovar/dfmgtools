'''
Various utils for smoothing plots.
'''

from numpy import *
import numpy
from numpy import median

__all__ = ['signal_smooth',
           'gaussian_smooth',
           'lowess_smooth',
           ]

# From http://www.scipy.org/Cookbook/SignalSmooth
def signal_smooth(x,window_len=11,window='hanning'):
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."
    if x.size < window_len:
        window_len = x.size
        #raise ValueError, "Input vector needs to be bigger than window size."
    if window_len<3:
        return x
    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
    s=numpy.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')
    y=numpy.convolve(w/w.sum(),s,mode='same')
    return y[window_len:-window_len+1]


def gaussian_smooth(x, y, w, xout=None):
    '''
    Does a Gaussian smooth with datapoint (x, y) and smoothing width w.
    
    Returns ysmooth.
    
    If xout is provided, ysmooth will correspond to xout, otherwise xout=x will be used.
    '''
    if xout is None:
        xout = x
    xout = reshape(xout, (1, len(xout)))
    x = reshape(x, (len(x), 1))
    weight = exp(-(x-xout)**2/(2*w**2))
    sumweight = sum(weight, axis=0)
    sumweight = reshape(sumweight, (1, len(sumweight)))
    weight /= sumweight
    xout = xout.flatten()
    y = reshape(y, (len(y), 1))
    return sum(y*weight, axis=0)

# from https://github.com/biopython/biopython/blob/master/Bio/Statistics/lowess.py
def lowess_smooth(x, y, f=2. / 3., iter=3):
    """lowess_smooth(x, y, f=2./3., iter=3) -> yest

    Lowess smoother: Robust locally weighted regression.
    The lowess function fits a nonparametric regression curve to a scatterplot.
    The arrays x and y contain an equal number of elements; each pair
    (x[i], y[i]) defines a data point in the scatterplot. The function returns
    the estimated (smooth) values of y.

    The smoothing span is given by f. A larger value for f will result in a
    smoother curve. The number of robustifying iterations is given by iter. The
    function will run faster with a smaller number of iterations.

    x and y should be numpy float arrays of equal length.  The return value is
    also a numpy float array of that length.

    e.g.
    >>> import numpy
    >>> x = numpy.array([4,  4,  7,  7,  8,  9, 10, 10, 10, 11, 11, 12, 12, 12,
    ...                 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15, 15, 16, 16,
    ...                 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 20, 20, 20, 20,
    ...                 20, 22, 23, 24, 24, 24, 24, 25], numpy.float)
    >>> y = numpy.array([2, 10,  4, 22, 16, 10, 18, 26, 34, 17, 28, 14, 20, 24,
    ...                 28, 26, 34, 34, 46, 26, 36, 60, 80, 20, 26, 54, 32, 40,
    ...                 32, 40, 50, 42, 56, 76, 84, 36, 46, 68, 32, 48, 52, 56,
    ...                 64, 66, 54, 70, 92, 93, 120, 85], numpy.float)
    >>> result = lowess(x, y)
    >>> len(result)
    50
    >>> print "[%0.2f, ..., %0.2f]" % (result[0], result[-1])
    [4.85, ..., 84.98]
    """
    n = len(x)
    r = int(numpy.ceil(f * n))
    h = [numpy.sort(abs(x - x[i]))[r] for i in range(n)]
    w = numpy.clip(abs(([x] - numpy.transpose([x])) / h), 0.0, 1.0)
    w = 1 - w * w * w
    w = w * w * w
    yest = numpy.zeros(n)
    delta = numpy.ones(n)
    for iteration in range(iter):
        for i in xrange(n):
            weights = delta * w[:, i]
            weights_mul_x = weights * x
            b1 = numpy.dot(weights, y)
            b2 = numpy.dot(weights_mul_x, y)
            A11 = sum(weights)
            A12 = sum(weights_mul_x)
            A21 = A12
            A22 = numpy.dot(weights_mul_x, x)
            determinant = A11 * A22 - A12 * A21
            beta1 = (A22 * b1 - A12 * b2) / determinant
            beta2 = (A11 * b2 - A21 * b1) / determinant
            yest[i] = beta1 + beta2 * x[i]
        residuals = y - yest
        s = median(abs(residuals))
        delta[:] = numpy.clip(residuals / (6 * s), -1, 1)
        delta[:] = 1 - delta * delta
        delta[:] = delta * delta
    return yest

if __name__=='__main__':
    from pylab import *
    
    if 1:
        t = linspace(-0.025, 0.025, 1000)
        yt = sin(2*pi*1000*t)
        y = yt+randn(len(t))*0.0
        ys = lowess_smooth(t, y, f=0.01)
        plot(t, yt)
        plot(t, y)
        plot(t, ys)
    
    if 0:

        subplot(211)
        x = linspace(-1, 1, 30)
        y = x**2
        yn = y+randn(len(y))*0.1
        z = signal_smooth(yn)
        plot(x, y, '-r')
        plot(x, yn, '-b')
        plot(x, z, '-g')
        
        subplot(212)
        x = 2*rand(30)-1
        x2 = linspace(-1, 1, 30)
        y = x**2
        y2 = x2**2
        yn = y+randn(len(y))*0.1
        z = gaussian_smooth(x, yn, 0.1, xout=x2)
        plot(x, yn, 'or')
        plot(x2, y2, '-r')
        plot(x2, z, '-g')
        
    show()
    