from numpy import *

__all__ = ['get_spikes_from_trains',
           'get_trains_from_spikes',
           'get_isi_from_spikes',
           'get_isi_from_trains',
           ]

def get_spikes_from_trains(spiketrains):
    i = []
    for j, st in enumerate(spiketrains):
        i.append(ones(len(st), dtype=int)*j)
    if len(spiketrains)==0:
        t = array([])
        i = array([], dtype=int)
    else:            
        t = hstack(spiketrains)
        i = hstack(i)
    return i, t

def get_trains_from_spikes(i, t, imax=None):
    if len(i):
        if imax is None:
            imax = amax(i)+1
        I = argsort(i)
        i = i[I]
        t = t[I]
        splitpoints, = hstack((diff(i),True)).nonzero()
        splitarray = split(t, splitpoints+1)
        trains = [array([]) for _ in xrange(imax)]
        for j, st in zip(i[splitpoints], splitarray):
            st.sort()
            trains[j] = st
        return trains
    else:
        if imax is None:
            imax = 1
        return [array([]) for _ in xrange(imax)]
    
    
def get_isi_from_trains(trains, flat=True):
    if flat:
        t = (2*len(trains)-1)*[nan]
        t[::2] = [sort(s) for s in trains]
        t = hstack(t)
        isi = diff(t)
        return isi[-isnan(isi)]
    else:
        return [diff(t) for t in trains]
    
def get_isi_from_spikes(i, t, imax=None, flat=True):
    return get_isi_from_trains(get_trains_from_spikes(i, t, imax=imax), flat=flat)
    
if __name__=='__main__':
    from numpy.random import *
    from pylab import hist, show
    i = randint(10000, size=(1000000,))
    t = rand(len(i))
    from time import time
    start = time()
    trains = get_trains_from_spikes(i, t)
    print time()-start
    i, t = get_spikes_from_trains(trains)
    trains2 = get_trains_from_spikes(i, t)
    i2, t2 = get_spikes_from_trains(trains)
    print amax(abs(i-i2)), amax(abs(t-t2))
    isi = get_isi_from_trains(trains)
    hist(isi, 100)
    show()
    