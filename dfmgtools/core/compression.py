'''
Load and save compressed pickle files.
'''

import cPickle as pickle
import zlib

__all__ = ['save_compressed', 'load_compressed']

def save_compressed(obj, file):
    if isinstance(file, str):
        file = open(file, 'wb')
    file.write(zlib.compress(pickle.dumps(obj, -1), 9))
    
def load_compressed(file):
    if isinstance(file, str):
        file = open(file, 'rb')
    return pickle.loads(zlib.decompress(file.read()))
