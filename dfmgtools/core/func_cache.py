'''
This provides a little wrapper around joblib, giving a default cache directory
which is three levels up from where the module is defined with a directory joblib_cache.
It also adds two convenience functions `is_cacheable` and `is_cached` to check if a
function was wrapped with joblib, and to check if a particular set of keywords and
arguments are already cached.
'''
import joblib
import os, inspect

__all__ = ['cache_path', 'cache_mem', 'func_cache', 'is_cacheable', 'is_cached']

basepath, _ = os.path.split(__file__)
cache_path = os.path.normpath(os.path.join(basepath, '../../../joblib_cache'))

cache_mem = joblib.Memory(cachedir=cache_path, verbose=0)

func_cache = cache_mem.cache

def is_cacheable(func):
    return hasattr(func, 'get_output_dir')

def is_cached(func, *args, **kwds):
    s = func.get_output_dir(*args, **kwds)
    return os.path.exists(func.get_output_dir(*args, **kwds)[0])

if __name__=='__main__':
    #@func_cache()
    @cache_mem.cache
    def f(x):
        return x*x*x
    
    print f(2)
    print f(2)
    