'''
Various file path tools.
'''

import os, sys, fnmatch
from cStringIO import StringIO

__all__ = ['in_directory',
           'RedirectStdStreams',
           'std_silent',
           'ensure_directory_of_file',
           'ensure_directory',
           'pattern_walk',
           ]

class in_directory(object):
    '''
    Safely temporarily work in a subdirectory
    
    Usage::
    
        with in_directory(directory):
            ... do stuff here
            
    Guarantees that the code in the with block will be executed in directory,
    and that after the block is completed we return to the original directory.
    '''
    def __init__(self,new_dir):
        self.orig_dir = os.getcwd()
        self.new_dir = new_dir
    def __enter__(self):
        os.chdir(self.new_dir)
    def __exit__(self,*exc_info):
        os.chdir(self.orig_dir)

class RedirectStdStreams(object):
    def __init__(self, stdout=None, stderr=None):
        if stdout=='null':
            stdout = open(os.devnull, 'w')
        if stderr=='null':
            stderr = open(os.devnull, 'w')
        self._stdout = stdout or sys.stdout
        self._stderr = stderr or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush(); self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr

    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush(); self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr


class std_silent(RedirectStdStreams):
    def __init__(self, alwaysprint=False):
        self.alwaysprint = alwaysprint
        self.newout = StringIO()
        self.newerr = StringIO()
        RedirectStdStreams.__init__(self,
                                    stdout=self.newout,
                                    stderr=self.newerr)
    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None or self.alwaysprint:
            self.newout.flush()
            self.newerr.flush()
            self.old_stdout.write(self.newout.getvalue())
            self.old_stderr.write(self.newerr.getvalue())
            self.old_stdout.flush()
            self.old_stderr.flush()
        RedirectStdStreams.__exit__(self, exc_type, exc_value, traceback)
        

def ensure_directory_of_file(f):
    '''
    Ensures that the directory of a given filename exists (creates it if necessary)
    '''
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)
    return d

        
def ensure_directory(d):
    '''
    Ensures that a given directory exists (creates it if necessary)
    '''
    if not os.path.exists(d):
        os.makedirs(d)
    return d


def pattern_walk(basefolder, pattern='*'):
    '''
    Yields all files recursively from a base folder matching pattern.
    
    Returns triples (fullname, base, name) where fullname is the full normalised path name,
    base is the directory, and name is the filename.
    '''
    basefolder = os.path.expanduser(basefolder)
    for dirpath, dirnames, filenames in os.walk(basefolder):
        filenames = fnmatch.filter(filenames, pattern)
        for filename in filenames:
            base, f = os.path.split(filename)
            fullname = os.path.normpath(os.path.join(dirpath, filename))
            yield fullname, base, f

if __name__=='__main__':
    import warnings
    with std_silent():
        print 'test1'
        warnings.warn("Warning 1!")
    with std_silent():
        print 'test2'
        warnings.warn("Warning 2!")
        raise ValueError
    