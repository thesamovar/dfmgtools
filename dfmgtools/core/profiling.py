import cProfile as profile
import pstats

__all__ = ['QuickProfile']

class QuickProfile():
    def __init__(self,
                 num=50,
                 sort_order=('cumulative', 'time', 'calls'),
                 ):
        self.prof = profile.Profile()
        self.sort_order = sort_order
        self.num = num
    def __enter__(self):
        self.prof.enable()
    def __exit__(self, *exc):
        self.prof.disable()
        stats = pstats.Stats(self.prof)
        stats.strip_dirs()
        stats.sort_stats(*self.sort_order)
        stats.print_stats(self.num)

if __name__=='__main__':
    def f(x):
        return x**2
    with QuickProfile(num=2):
        for i in xrange(1000000):
            f(i)
