'''
StructuredObject is just an empty heap class with some nice formatting. There is also a function
here to automatically load in Matlab structs as loaded by scipy's loadmat.
'''

from numpy import *
from scipy import *
from scipy.io import *
import os, sys
from scipy.io import savemat
from brian import Quantity
from brian.hears import dB_type

__all__ = ['StructuredObject',
           'structured_object_from_matlab_variable',
           'structured_object',
           ]

   
def make_matlab_compatible(obj, longnames=True):
    if isinstance(obj, (Quantity, dB_type)):
        return float(obj)
    if isinstance(obj, ndarray) and obj.dtype==object and len(obj.shape)==1:
        return array([make_matlab_compatible(o,
                                 longnames=longnames) for o in obj])
    if not isinstance(obj, StructuredObject):
        return obj
    newobj = StructuredObject()
    for k, v in obj.__dict__.iteritems():
        if not k.startswith('_'):
            if not longnames:
                if len(k)>31:
                    k = k[:26]+'_'+k[-4:]
            if hasattr(newobj, k):
                raise ValueError("Problem with shortening names")
            setattr(newobj, k, make_matlab_compatible(v, longnames=longnames))
    return newobj


class StructuredObject(object):
    def __init__(self, matlabvar=None):
        if matlabvar is not None:
            pass
        
    def apply_filter(self, filter='', basename='', case_insensitive=True):
        newobj = StructuredObject()
        for name in self._get_names():
            var = getattr(self, name)
            if basename:
                fullname = basename+'.'+name
            else:
                fullname = name
            if isinstance(var, StructuredObject):
                subobj = var.apply_filter(filter=filter, basename=fullname)
                if len(subobj._get_names()):
                    setattr(newobj, name, subobj)
            elif isinstance(var, ndarray) and var.dtype==object:
                shape = var.shape
                subobj = var.flatten()
                numnames = 0
                for i in xrange(subobj.size):
                    if isinstance(subobj[i], StructuredObject):
                        subobj[i] = subobj[i].apply_filter(filter=filter, basename=fullname)
                        numnames += len(subobj[i]._get_names())
                if numnames:
                    setattr(newobj, name, subobj)
            elif case_insensitive and filter.lower() in repr(var).lower():
                setattr(newobj, name, var)
            elif not case_insensitive and filter in repr(var):
                setattr(newobj, name, var)
            else:
                if case_insensitive and filter.lower() in fullname.lower():
                    setattr(newobj, name, var)
                if not case_insensitive and filter in fullname:
                    setattr(newobj, name, var)
        return newobj
            
    def _get_names(self):
        return [name for name in self.__dict__ if not name.startswith('_')]
        
    def save_matlab(self, fname, name='data', longnames=True, oned_as='row'):
        obj = make_matlab_compatible(self, longnames=longnames)
        savemat(fname, {name: obj}, long_field_names=longnames, oned_as=oned_as)
        
    def _format(self, showvalues=False, showallobjects=False, indent=0):
        s = ''
        for name in self._get_names():
            var = getattr(self, name)
            val = ''
            if isinstance(var, ndarray):
                if showvalues:
                    if len(var.shape)==1:
                        shape = str(var.shape[0])
                    else:
                        shape = str(var.shape)                            
                    typename = 'array of {shape} {dtype}'.format(
                        dtype=str(var.dtype), shape=shape)
                else:
                    typename = '{N}D array of {dtype}'.format(
                        dtype=str(var.dtype), N=len(var.shape))                        
            elif isinstance(var, StructuredObject):
                typename = 'struct'
            else:
                typename = var.__class__.__name__
                if showvalues:
                    if isinstance(var, str):
                        val = ' = '+repr(var)
                    else:
                        val = ' = '+str(var)
            s += '{name}{val} [{typename}]\n'.format(
                name=name, val=val, typename=typename)
            if isinstance(var, StructuredObject):
                s += var._format(showvalues=showvalues,
                                 showallobjects=showallobjects,
                                 indent=1)
            if isinstance(var, ndarray) and var.size:
                var0 = var.flat[0]
                if isinstance(var0, StructuredObject):
                    if (not showvalues and not showallobjects) or (var.size==1 and showvalues):
                        s += var0._format(showvalues=showvalues,
                                          showallobjects=showallobjects,
                                          indent=1)
                    elif showvalues and showallobjects:
                        for i, v in ndenumerate(var):
                            if len(i)==1:
                                i, = i
                            s += '    {name}[{i}]:\n'.format(name=name, i=i)
                            s += v._format(showvalues=showvalues,
                                           showallobjects=showallobjects,
                                           indent=2)
        if indent:
            s = '\n'.join('    '*indent+line for line in s.split('\n') if line)+'\n'
        return s
    
    def __str__(self):
        return self._format(showvalues=True, showallobjects=False)
    
    def __repr__(self):
        return self._format(showvalues=True, showallobjects=True)


def structured_object_from_matlab_variable(var, nosqueeze=None, varname=''):
    if nosqueeze is None:
        nosqueeze = set()
    # parse void struct
    if var.__class__ is void:
        out = StructuredObject()
        for name in var.dtype.names:
            setattr(out, name,
                    structured_object_from_matlab_variable(var[name], nosqueeze, name))
        return out
    # parse 1x1 struct array
    if var.shape==(1, 1) and var.dtype.names is not None:
        out = StructuredObject()
        for name in var.dtype.names:
            setattr(out, name,
                    structured_object_from_matlab_variable(var[0, 0][name], nosqueeze, name))
        return out
    # parse 1xN or Nx1 struct array
    if var.dtype.names is not None:
        if len(var.shape)==2 and (var.shape[0]==1 or var.shape[1]==1):
            return array([structured_object_from_matlab_variable(x, nosqueeze) for x in var.flat],
                         dtype=object)
    # parse cell array
    if var.dtype==object:
        if len(var.shape)==1 or (len(var.shape)==2 and (var.shape[0]==1 or var.shape[1]==1)):
            return array([structured_object_from_matlab_variable(x, nosqueeze) for x in var.flat],
                         dtype=object)
    # parse 1x1 value array
    if varname not in nosqueeze and var.shape==(1, 1) and var.dtype.names is None and var.dtype!=object:
        return var[0, 0]
    # parse string
    if var.shape==(1,) and var.dtype.names is None and var.dtype!=object:
        return str(var[0])
    # default
    return var


def structured_object(**kwds):
    obj = StructuredObject()
    for k, v in kwds.iteritems():
        setattr(obj, k, v)
    return obj
