'''
Matlab auditory model class.
'''
from dfmgtools.core import *
import os
#import mlabwrap # this is imported only when needed
mlabwrap = None

__all__ = ['MatlabModel']

thisdir, _ = os.path.split(__file__)

class MatlabModel(object):
    uses_matlab = True
    # USER SHOULD OVERRIDE THIS
    # Use self.setup_matlab_function(name, numargs) for each matlab function
    # to import, they will then be available as self.name()
    def get_matlab_functions(self):
        pass
    # USERS SHOULD OVERRIDE THESE
    # if multichannel is supported, args should be (sound, channels, cfs)
    # otherwise args should be (sound, cfs). In both cases, cfs is the array of
    # central frequencies, and sound is the sound to apply. In the single
    # channel case, sound will have a single channel. In the multichannel case,
    # channels is an array of ints, the same length as cfs, which gives the
    # channel index in the sound. In all cases, it should return a list of
    # spiketrains (arrays of spike times) of length len(cfs). You can use the
    # spiketrains.get_trains_from_spikes if necessary.
    multichannel = False
    maxblocksize = 1
    def __call__(self, *args, **params):
        pass

    def setup_matlab_function(self, name, numreturnvalues):
        if not name in mlabwrap.mlab.__dict__:
            func = mlabwrap.mlab._make_mlab_command(name, numreturnvalues, '')
            setattr(mlabwrap.mlab, name, func)
        if not hasattr(self, name):
            setattr(self, name, getattr(mlabwrap.mlab, name))
    
    def import_mlabwrap(self):
        global mlabwrap
        with RedirectStdStreams(stdout='null', stderr='null'):
            if mlabwrap is None:
                import mlabwrap
            if not hasattr(self, 'seed_urandom'):
                with in_directory(os.path.join(thisdir, '..')):
                    self.setup_matlab_function('seed_urandom', 0)
            self.get_matlab_functions()

    def init(self):
        self.import_mlabwrap()
        with in_directory(os.path.join(thisdir, '..')):
            self.seed_urandom()
        self.prepared = True
