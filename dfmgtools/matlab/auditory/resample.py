'''
Resample sound files with Matlab
'''

from brian import *
from brian.hears import *

mlabwrap = None # delayed import

__all__ = ['resample_with_matlab']


def resample_with_matlab(sound, samplerate):
    global mlabwrap
    import mlabwrap
    newsound = mlabwrap.mlab.resample(asarray(sound), int(samplerate),
                                      int(sound.samplerate), nout=1)
    return Sound(newsound, samplerate=samplerate)


if __name__=='__main__':
    sound = tone(100*Hz, duration=100*ms, samplerate=1*kHz)
    sound2 = resample_with_matlab(sound, 5*kHz)
    sound3 = tone(100*Hz, duration=100*ms, samplerate=5*kHz)
    plot(sound.times, sound)
    plot(sound2.times, sound2)
    plot(sound3.times, sound3)
    show()
    