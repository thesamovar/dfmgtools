'''
Read wav files with Matlab.
'''

from brian import *
from brian.hears import *

mlabwrap = None # delayed import

__all__ = ['read_wav_with_matlab']

def read_wav_with_matlab(wavname):
    global mlabwrap
    import mlabwrap
    snd, fs = mlabwrap.mlab.wavread(wavname, nout=2)
    return Sound(snd, samplerate=float(fs)*Hz)
