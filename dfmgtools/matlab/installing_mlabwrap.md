Notes on installing mlabwrap on Windows
=======================================

WARNING: installing mlabwrap will be annoying and will take most of the day, but following the
         instructions and suggestions below you will probably get there in the end.

NOTE: at the moment, I can't use mlabwrap on multiple processes in Windows,
and it seems like other people are having the same problem

NOTE: on more recent versions of matlab, you might need to run matlab /regserver at a command line
      before it will work.

In my case, I downloaded the 1.1 version of mlabwrap, then downloaded the
mlabraw.cpp files from this page:

http://obasic.net/how-to-install-mlabwrap-on-windows

I also made this change to setup.py from that page:

	# In line 185 change the code from
	runtime_library_dirs=MATLAB_LIBRARY_DIRS,
	# To (Yes, comment it!)
	#runtime_library_dirs=MATLAB_LIBRARY_DIRS,
	
And one additional change of my own:

    Change:
    	error = call(cmd, **extra_args)
    To:
        error = os.system(' '.join(cmd))


Finally, I needed to copy two .lib files libeng.lib and libmx.lib from
Matlab to my Visual Studio 9.0 lib folder. With this done, it worked.

You might also need to add the path to the Matlab DLL files directory to
your path (matlab/bin/win32), and in my case copy python27.dll from
Windows/SysWOW64 to Windows/System32.

For Python 2.7 and Windows32 with Matlab 2013a built with VC Express 10.0, I had to copy some
additional .lib files (you'll see these as you try to install), modify the setup script to
point directly to the VC Express 10.0 directory, modify Python27/Lib/distutils/msvc9compiler.py to
point directly to the VC directory, and modify Python27/include/pyconfig.h to remove the 
line #include <basetsd.h> (which is not needed). 

For using mlabwrap on 64 bit windows, there are some additional tricks. You
need to install the 32 and 64 bit versions of matlab (just the 32 bit version
ought to be fine in principle, but seems not to work unless the 64 bit version
is also present for some reason). Make sure to install the 64 bit version 
before the 32 bit version.

For using mlabwrap, the following tips might be useful.

Firstly, when calling a non-library Matlab function, rather than doing:

	a, b = mlabwrap.mlab.funcname(...)
	
Do this once at the start:

	funcname = mlabwrap.mlab._make_mlab_command('funcname', 2, '')
	# where 2 is the number of output arguments
	setattr(mlabwrap.mlab, 'funcname', funcname)
	# if you don't do this second step it crashes, probably because of memory
	# deallocation issues
	
And then just call funcname() when you need it.

You also probably need to work from the Matlab directory where the function
is defined, one way of doing that is to use the in_directory() context manager
defined in dfmgtools.core.paths, e.g.:

	# find the directory of the current file
	thisdir, _ = os.path.split(__file__)
	# work temporarily in this directory, and afterwards return to the normal
	# working directory
	with in_directory(thisdir):
	    catmodel_IHC = mlabwrap.mlab._make_mlab_command('catmodel_IHC', 1, '')
	    catmodel_Synapse = mlabwrap.mlab._make_mlab_command('catmodel_Synapse', 2, '')
	    setattr(mlabwrap.mlab, 'catmodel_IHC', catmodel_IHC)
	    setattr(mlabwrap.mlab, 'catmodel_Synapse', catmodel_Synapse)

One last thing is to temporarily redirect stdout to null, which you can do
with the RedirectStdStreams context manager in hearfin.utils, like so:

    with RedirectStdStreams(stdout='null'):
    	...

Notes on installing mlabwrap on Linux 64 bit
============================================

I used:

http://sourceforge.net/mailarchive/message.php?msg_id=27312822

Note, make a .profile file with command:

export MLABRAW_CMD_STR="/path/to/matlab -nosplash"

