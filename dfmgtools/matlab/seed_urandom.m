function seed_urandom()
if isunix
    [status seed] = system('od /dev/urandom --read-bytes=4 -tu | awk ''{print $2}''');
    seed=str2double(seed);
    RandStream.setDefaultStream(RandStream('mt19937ar','seed',seed));
else
    stream = RandStream('mt19937ar','Seed',int32(sum(100*clock))+feature('GetPid'));
    [major, minor] = sscanf(version, '%d.%d', 2);
    major = major(1, 1);
    if (major>7) | ((major==7) & (minor>=12))
        RandStream.setGlobalStream(stream);
    else
        RandStream.setDefaultStream(stream);
    end
end