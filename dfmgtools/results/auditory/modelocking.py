from pylab import *

__all__ = ['modelocking_figure']

def modelocking_figure(trains, fig=None, ax=None, interval=None):
    if fig is None and ax is None:
        ax = gca()
    if fig is not None and ax is None:
        ax = fig.add_subplot(111)
    if interval is not None:
        start, end = interval
        newtrains = []
        for train in trains:
            train = train[logical_and(train>=start, train<end)]
            newtrains.append(train)
        trains = newtrains
    x = []
    y = []
    for train in trains:
        isi = diff(train)
        x.append(isi[:-1])
        y.append(isi[1:])
    x = hstack(x)
    y = hstack(y)
    
    ax.plot(x, y, '.k', ms=2)
    ax.set_xlabel('First ISI (s)')
    ax.set_ylabel('Second ISI (s)')
    
    return ax
    