from brian import *
from dfmgtools.core import *
from dfmgtools.compute import *
from matplotlib import gridspec
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
from collections import defaultdict
from numpy import interp

__all__ = ['stat_figure',
           'compute_psth',
           'psth_figure',
           'multi_psth_figure',
           'raster_figure',
           'isi_cv',
           'compute_isi_histogram_cv',
           'isi_figure',
           'isi_all_figure',
           'binned_isi',
           'binned_isi_figure',
           ]

def stat_figure(i, t, histwidth=0.1*ms, markersize=2, window=100*ms,
                binwidth=0.5*ms, minbinpoints=5,
                isi_segment=None, duration=None,
                isi_max=None, rate_max=None,
                adapted_rate_segment=None,
                raster_max=5000,
                fig=None, gs=None,):
    if fig is None:
        fig = gcf()
    if gs is None:
        gs = gridspec.GridSpec(4, 2)
    else:
        gs = gridspec.GridSpecFromSubplotSpec(4, 2, subplot_spec=gs)
    if len(i)==0:
        return
    figaxes = {}
    ax = ax_main = figaxes['main'] = fig.add_subplot(gs[0:2, 1])
    psth_figure(i, t, histwidth=histwidth, ax=ax, duration=duration,
                rate_max=rate_max, time_unit=second,
                adapted_rate_segment=adapted_rate_segment)
    ax = figaxes['raster'] = fig.add_subplot(gs[0:2, 0], sharex=ax)
    raster_figure(i, t, markersize=markersize, ax=ax, duration=duration,
                  raster_max=raster_max, segment=isi_segment, time_unit=second)
    ax = figaxes['isi_hist'] = fig.add_subplot(gs[2, 1])
    isi_figure(i, t, histwidth=histwidth, ax=ax, segment=isi_segment)
    if isi_max is not None:
        ax.set_xlim(0, float(isi_max))
    ax = figaxes['isi_hist_all'] = fig.add_subplot(gs[3, 1], sharex=ax)
    isi_all_figure(i, t, histwidth=histwidth, window=window, ax=ax,
                   segment=isi_segment)    
    if isi_max is not None:
        ax.set_xlim(0, float(isi_max))
    ax_isi = figaxes['isi'] = fig.add_subplot(gs[2, 0], sharex=ax_main)
    ax_cv = figaxes['cv'] = fig.add_subplot(gs[3, 0], sharex=ax_main)
    trains = get_trains_from_spikes(i, t)
    binned_isi_figure(trains, binwidth=binwidth, minpoints=minbinpoints,
                      ax_isi=ax_isi, ax_cv=ax_cv, duration=duration,
                      time_unit=second)
    if duration is not None:
        ax_main.set_xlim(0, float(duration))
    return figaxes

def compute_psth(i, t, duration, histwidth=0.1*ms):
    count = zeros(int(duration/histwidth))
    if len(i)==0:
        return count
    N = amax(i)
    if len(unique(t))<=1:
        return count
    bc = bincount(array(t/histwidth, dtype=int))
    if len(bc)>len(count):
        bc = bc[:len(count)]
    count[:len(bc)] = bc
    return count

def adapted_rate(i, t, low, high):
    if len(i)==0:
        return 0*Hz
    N = amax(i)+1
    I = logical_and(t>=low, t<=high)
    i = i[I]
    t = t[I]
    if len(i)==0:
        return 0*Hz
    return len(i)*1.0/(N*(high-low))

def psth_figure(i, t,
                histwidth=0.1*ms,
                markersize=2,
                duration=None,
                ax=None,
                rate_max=None,
                adapted_rate_segment=None,
                fig=None,
                time_unit=ms,
                c='k',
                ):
    if ax is None and fig is None:
        ax = gca()
    elif ax is None:
        ax = fig.add_subplot(111)
    if len(i)==0:
        return
    N = amax(i)
    if len(unique(t))>1:
        try:
            ax.hist(array(t/time_unit), int(amax(t/time_unit)/(histwidth/time_unit)),
                    weights=(1/histwidth)*ones(len(t))/N,
                    histtype='stepfilled', color=c, ec='none')
        except ValueError:
            pass
    if adapted_rate_segment is not None:
        low, high = adapted_rate_segment
        ar = adapted_rate(i, t, low, high)
        ax.plot([low/time_unit, high/time_unit], [ar, ar], '-r')
    if duration is not None:
        xlim(xmin=0, xmax=float(duration/time_unit))
    if rate_max is not None:
        ylim(ymin=0, ymax=rate_max)
    if adapted_rate_segment is not None:
        msg = 'AR=%.1f Hz'%float(ar)
        ax.add_artist(AnchoredText(msg, frameon=True, loc=1,
                                   prop={'color':'r'}))        
    ax.set_xlabel('Time (%s)' % str(time_unit))
    ax.set_ylabel('Rate (Hz)')
    return ax

def multi_psth_figure(trains, histwidth=0.25*ms, segment=None, nspikes=5,
                      rate_max=None,
                      lw=1, alpha=0.8,
                      ax=None, fig=None):
    if ax is None and fig is None:
        ax = gca()
    elif ax is None:
        ax = fig.add_subplot(111)
    T = defaultdict(list)
    N = len(trains)
    for train in trains:
        if segment is not None:
            start, end = segment
            train = train[logical_and(train>=start, train<end)]
        for i in xrange(min(nspikes, len(train))):
            T[i].append(train[i])
    for i in xrange(nspikes):
        t = array(T[i])
        if len(t)>1:
            s = std(t/ms)
            ax.hist(t, int((amax(t)-amin(t))/histwidth),
                    weights=(1/histwidth)*ones(len(t))/N,
                    histtype='stepfilled', alpha=alpha, ec='none',
                    label=r'Spike %d: $\sigma$=%.2f ms'%(i+1, s))
    if segment is not None:
        start, end = segment
        ax.set_xlim(float(start), float(end))
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Rate (Hz)')
    ax.legend()
    if rate_max is not None:
        ax.set_ylim(0, float(rate_max))
    return ax
    
def raster_figure(i, t, markersize=2,
                  duration=None,
                  segment=None,
                  showfirstcol='r',
                  raster_max=5000,
                  time_unit=ms,
                  ax=None, fig=None):
    if ax is None and fig is None:
        ax = gca()
    elif ax is None:
        ax = fig.add_subplot(111)
    n = amax(i)
    if n>=raster_max:
        I = (i%int(ceil((n*1.0)/raster_max)))==0
        i = i[I]
        t = t[I]
    ax.plot(t/time_unit, i, '.k', ms=markersize)
    if showfirstcol is not None and len(i):
        allfirst = []
        for j in xrange(amax(i)+1):
            s = t[i==j]
            if segment is not None:
                low, high = segment
                s = s[logical_and(s>=low, s<=high)]
            if len(s):
                s = amin(s)
                allfirst.append(s)
                ax.plot([s], [j], 'o', ms=markersize, color=showfirstcol,
                        mec=showfirstcol)
        if len(allfirst)>2:
            mufs = mean(allfirst)
            stdfs = std(array(allfirst)/ms)
            pfs = len(allfirst)*1.0/(amax(i)+1)
            fsmsg = '$\\sigma_{fs}=%.2f$ ms'%stdfs
            ax.add_artist(AnchoredText(fsmsg, frameon=True, loc=1,
                                       prop={'color':'r'}))
    if duration is not None:
        ax.set_xlim(xmin=0, xmax=float(duration/time_unit))
    ax.set_xlabel('Time (%s)' % str(time_unit))
    ax.set_ylabel('Trial')
    return ax

def isi_cv(i, t, segment=None):
    if segment is not None:
        start, end = segment
        I = logical_and(t>=start, t<=end)
        t = t[I]
        i = i[I]
    if len(i)==0:
        return nan
    all_isi = []
    for j in xrange(amax(i)):
        s = sort(t[i==j])
        all_isi.append(diff(s))
    all_isi = hstack(all_isi)
    if len(all_isi)<=1:
        return nan
    return std(all_isi)/mean(all_isi)

def compute_isi_histogram_cv(i, t, isi_max, histwidth=.1*ms, segment=None):
    count = zeros(int(isi_max/histwidth))
    if len(i)==0:
        return count, 0.0
    if segment is not None:
        start, end = segment
        I = logical_and(t>=start, t<=end)
        t = t[I]
        i = i[I]
    if len(i)==0:
        return count, 0.0
    all_isi = []
    for j in xrange(amax(i)):
        s = sort(t[i==j])
        all_isi.append(diff(s))
    all_isi = hstack(all_isi)
    if len(all_isi):
        bc = bincount(array(all_isi/histwidth, dtype=int))
        if len(bc)>len(count):
            bc = bc[:len(count)]
        count[:len(bc)] = bc
        cv = std(all_isi)/mean(all_isi)
        return count, cv
    else:
        return count, 0.0        

def isi_figure(i, t, histwidth=.1*ms,
               segment=None,
               ax=None, fig=None):
    if ax is None and fig is None:
        ax = gca()
    elif ax is None:
        ax = fig.add_subplot(111)
    if len(i)==0:
        return
    if segment is not None:
        start, end = segment
        I = logical_and(t>=start, t<=end)
        t = t[I]
        i = i[I]
    if len(i)==0:
        return
    all_isi = []
    for j in xrange(amax(i)):
        s = sort(t[i==j])
        all_isi.append(diff(s))
    all_isi = hstack(all_isi)
    if len(all_isi):
        a, b = amin(all_isi), amax(all_isi)
        nbins = int((b-a)/histwidth)
        if nbins==0:
            nbins = 1
        ax.hist(all_isi, nbins,
             normed=True,
             histtype='stepfilled', color='k', ec='none')
    ax.set_yticks([])
    ax.set_xlabel('First order ISI (s)')
    ax.set_ylabel('Density')
    cv = std(all_isi)/mean(all_isi)
    ax.add_artist(AnchoredText("CV = %.2f"%cv, frameon=False, loc=1, prop={}))
    return ax

def isi_all_figure(i, t, histwidth=.1*ms, window=100*ms,
                   segment=None,
                   ax=None, fig=None):
    if ax is None and fig is None:
        ax = gca()
    elif ax is None:
        ax = fig.add_subplot(111)
    if len(i)==0:
        return
    if segment is not None:
        start, end = segment
        I = logical_and(t>=start, t<=end)
        t = t[I]
        i = i[I]
    if len(i)==0:
        return
    all_isi = []
    if len(i):
        amaxi = amax(i)
    else:
        amaxi = 0
    for j in xrange(amaxi):
        s = sort(t[i==j])
        s = reshape(s, (-1, 1))-reshape(s, (1, -1))
        u, v = tril_indices(len(s), -1)
        s = s[u, v]
        s = s[abs(s)<window]
        all_isi.append(s)
    if len(all_isi):
        all_isi = hstack(all_isi)
    if len(all_isi):
        a, b = amin(all_isi), amax(all_isi)
        nbins = int((b-a)/histwidth)
        try:
            ax.hist(all_isi, nbins,
                 normed=True,
                 histtype='stepfilled', color='k', ec='none')
        except ValueError:
            pass
    ax.set_yticks([])
    ax.set_xlabel('All-order ISI (s)')
    ax.set_ylabel('Density')
    return ax


def interpnan(x, externalfill=nan):
    '''
    Interpolates interior nans, fills exterior nans with externalfill
    '''
    y = ones(len(x))*externalfill
    I = isnan(x)
    if sum(I)==len(x):
        return y
    y[-I] = x[-I]
    start = (-I).nonzero()[0][0]
    end = (-I).nonzero()[0][-1]+1
    z = x[start:end]
    J = isnan(z)
    w = J.nonzero()[0]
    y[J.nonzero()[0]+start] = interp(J.nonzero()[0], (-J).nonzero()[0], z[-J])
    return y


def binned_isi(spiketrains, onset, offset, binwidth=.5*ms, minpoints=5, interpolate=True):
    nbin = int(ceil((offset-onset)/binwidth))+1
    bmean = zeros(nbin)
    bstd = zeros(nbin)
    bmean[:] = nan
    bstd[:] = nan
    btime = arange(nbin)*binwidth+binwidth*0.5+onset
    if len(spiketrains)==0:
        return btime, bstd, bmean
    isi = []
    t = []
    for st in spiketrains:
        st = st[st>=onset]
        st = st[st<=offset]
        st = st-onset
        d = diff(st)
        isi.append(d)
        t.append(0.5*(st[:-1]+st[1:]))
    isi = hstack(isi)
    t = hstack(t)
    if len(t)==0:
        return btime, bstd, bmean
    I = argsort(t)
    t = t[I]
    isi = isi[I]
    bin = array(t/binwidth, dtype=int)
    binedges, = hstack((1, diff(bin))).nonzero()
    bin_num = bin[binedges]
    bin_time = bin_num*binwidth+0.5*binwidth
    num_in_bin = add.reduceat(ones(len(isi)), binedges)
    binmean = add.reduceat(isi, binedges)/num_in_bin
    binstd = sqrt(add.reduceat(isi**2, binedges)/num_in_bin-binmean**2)
    bmean[bin_num] = binmean
    bstd[bin_num] = binstd
    bmean[bin_num[num_in_bin<minpoints]] = nan
    bstd[bin_num[num_in_bin<minpoints]] = nan
    # If we are doing interpolation, we fill in all the interior nans with interpolated values,
    # and leave the external nans
    if interpolate:
        bstd = interpnan(bstd)
        bmean = interpnan(bmean)
    return btime, bstd, bmean


def binned_isi_figure(spiketrains, binwidth=.5*ms, minpoints=5,
                     fig=None, ax_isi=None, ax_cv=None,
                     duration=None,
                     time_unit=ms,
                     col_cv='b',
                     ):
    if ax_isi is None and ax_cv is None:
        if fig is None:
            fig = figure()
        ax_isi = fig.add_subplot(211)
        ax_cv = fig.add_subplot(212, sharex=ax_isi)
    if len(spiketrains)==0:
        return
    isi = []
    t = []
    for st in spiketrains:
        d = diff(st)
        isi.append(d)
        t.append(0.5*(st[:-1]+st[1:]))
    isi = hstack(isi)
    t = hstack(t)
    I = argsort(t)
    if len(t)==0:
        return
    t = t[I]
    isi = isi[I]
    bin = array(t/binwidth, dtype=int)
    binedges, = hstack((1, diff(bin))).nonzero()
    bin_num = bin[binedges]
    bin_time = bin_num*binwidth+0.5*binwidth
    num_in_bin = add.reduceat(ones(len(isi)), binedges)
    binmean = add.reduceat(isi, binedges)/num_in_bin
    binstd = sqrt(add.reduceat(isi**2, binedges)/num_in_bin-binmean**2)
    bmean = zeros(bin_num[-1]+1)
    bstd = zeros(bin_num[-1]+1)
    bmean[:] = nan
    bstd[:] = nan
    bmean[bin_num] = binmean
    bstd[bin_num] = binstd
    bmean[bin_num[num_in_bin<minpoints]] = nan
    bstd[bin_num[num_in_bin<minpoints]] = nan
    btime = arange(bin_num[-1]+1)*binwidth+binwidth*0.5
    # compute the points in the graph that will be shown (i.e points that are
    # not nans and where at least one of the neighbouring points is not a nan)
    I = hstack((True, isnan(bmean), True))
    I = -I[1:-1]&(-I[2:]|-I[:-2])
    if duration is not None:
        I = I&(btime<float(duration))
    if ax_isi is not None:
        ax_isi.plot(btime/time_unit, bmean, label=r'$\mu$')
        ax_isi.plot(btime/time_unit, bstd, label=r'$\sigma$')
        ax_isi.legend(loc='upper left', frameon=False)
        ax_isi.set_xlabel('Time (%s)' % str(time_unit))
        ax_isi.set_ylabel(r'ISI $\mu$ or $\sigma$ (s)')
        if sum(I):
            ax_isi.set_ylim(0, max(amax(bmean[I]), amax(bstd[I])))
        if duration is not None:
            ax_isi.set_xlim(0, float(duration/time_unit))
        label_ax = ax_isi
    if ax_cv is not None:
        ax_cv.plot(btime/time_unit, bstd/bmean, c=col_cv)
        ax_cv.set_xlabel('Time (%s)' % str(time_unit))
        ax_cv.set_ylabel('CV of ISI')
        if sum(I):
            ax_cv.set_ylim(0, amax(bstd[I]/bmean[I]))
        if duration is not None:
            ax_cv.set_xlim(0, float(duration/time_unit))
        label_ax = ax_cv
    return ax_isi, ax_cv

if __name__=='__main__':
    i = randint(1000, size=10000)
    t = rand(len(i))*0.05
    subplot(221)
    raster_figure(i, t)
    subplot(222)
    psth_figure(i, t)
    subplot(223)
    isi_figure(i, t)
    subplot(224)
    isi_all_figure(i, t)
    show()
    