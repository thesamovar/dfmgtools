from brian import *
from matplotlib import cm

__all__ = ['responsemap']

def responsemap(fmin, fmax, fN,
                levelmin, levelmax, levelN,
                rates,
                ratemin=0, ratemax=None,
                freqscale=log10,
                cmap=cm.hot, interpolation=None,
                majorlength=10, majorwidth=1,
                minorlength=5, minorwidth=0.75,
                tickcol='w',
                show_colorbar=True,
                major=None, minor=None,
                fig=None, ax=None):
    
    if fig is None and ax is None:
        fig = gcf()
    if ax is None:
        ax = fig.add_subplot(111)
   
    im = ax.imshow(rates,
              origin='lower left', aspect='auto', interpolation=interpolation,
              extent=(0, 1, levelmin, levelmax), cmap=cmap,
              vmin=ratemin, vmax=ratemax)
    
    if show_colorbar:
        fig.colorbar(im, ax=ax)
    
    def eligible(f):
        f = array(f)
        f = f[f>=fmin]
        f = f[f<=fmax]
        pos = (freqscale(f)-freqscale(fmin))/(freqscale(fmax)-freqscale(fmin))
        return list(f), list(pos)
    
    if major is None:
        major = [10, 100, 1000, 10000, 100000]
    if minor is None:
        minor = (range(20, 100, 10)+
                 range(200, 1000, 100)+
                 range(2000, 10000, 1000)+
                 range(20000, 100000, 10000))
    
    fmajor, fmajorpos = eligible(major)
    fminor, fminorpos = eligible(minor)
    
    xax = ax.xaxis
    xax.set_ticks(fmajorpos, minor=False)
    xax.set_ticklabels(map(str, fmajor))
    xax.set_ticks(fminorpos, minor=True)
    xax.set_tick_params('major', color=tickcol, length=majorlength,
                        width=majorwidth)
    xax.set_tick_params('minor', color=tickcol, length=minorlength,
                        width=minorwidth)
    ax.yaxis.set_tick_params('major', color=tickcol, length=majorlength,
                             width=majorwidth)
