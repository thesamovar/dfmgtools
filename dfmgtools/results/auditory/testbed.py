from brian import *
from brian.hears import *
import os, sys, shutil, pickle, inspect, hashlib, shelve
from collections import defaultdict
from dfmgtools.core.paths import ensure_directory

__all__ = ['Testbed']

class Testbed(object):
    def __init__(self, models, tests, results_dir,
                 png_dpi=100, thumbnail_dpi=24, name='testbed',
                 ):
        self.models = models
        self.tests = tests
        self.results_dir = results_dir
        self.png_dpi = png_dpi
        self.thumbnail_dpi = thumbnail_dpi
        self.name = name
        self.cache_name = os.path.join(results_dir, name+'.cache.shelf')
        ensure_directory(results_dir)
        self.shelf = shelve.open(self.cache_name)
        
    def clean(self):
        # remove directories that are not indexed
        modelsneeded, testsneeded = self.needed()
        keep_paths = set(self.paths.values())
        output_path = os.path.normpath(os.path.join(self.results_dir, self.name))
        keep_paths.add(output_path)
        for dirname, _, _ in os.walk(output_path):
            dirname = os.path.normpath(dirname)
            if dirname not in keep_paths:
                print 'Removing', dirname
                shutil.rmtree(dirname)
        
    def needed(self):
        # create testbed directory, and remove old one if it exists
        testbed_dir = os.path.join(self.results_dir, self.name)
        if not os.path.exists(testbed_dir):
            os.mkdir(testbed_dir)
        self.paths = {}
        self.results = {}
        # run through all models and tests, check what needs to be computed
        # by looking in the shelf
        modelsneeded = set()
        testsneeded = defaultdict(list)
        for model in self.models:
            model_name = model.uid
            model_dir = os.path.join(testbed_dir, model_name)
            self.paths[model] = model_dir
            for test in self.tests:
                test_name = test.uid
                test_dir = os.path.join(model_dir, test_name)
                if not os.path.exists(test_dir):
                    testremoved = True
#                    os.mkdir(test_dir)
                else:
                    testremoved = False
                self.paths[model, test] = test_dir
                key = model.hash()+test.hash()
                if testremoved and key in self.shelf:
                    del self.shelf[key]
                    self.shelf.sync()
                if key in self.shelf:
                    self.results[model, test] = self.shelf[key]
                else:
                    modelsneeded.add(model)
                    testsneeded[model].append((test, key))
        return modelsneeded, testsneeded
        
    def run(self, force=False):
        modelsneeded, testsneeded = self.needed()
        if not force and len(modelsneeded)==0:
            print 'Nothing to compute, if files have changed and cache not updating'
            print 'delete the file:'
            print self.cache_name
            print 'or delete individual model or test directories in:'
            print self.results_dir
            return
        # compute what needs to be computed
        print 'Computing updates for models', ', '.join(model.id for model in modelsneeded)
        for model in modelsneeded:
            tn = testsneeded[model]
            print 'Computing updates for model', model.id+', tests',
            print ', '.join(test.id for test, _ in tn)
            model.start()
            model_dir = self.paths[model]
            if not os.path.exists(model_dir):
                os.mkdir(model_dir)
            for test, key in tn:
                test_dir = self.paths[model, test]
                result = test.run(model)
                if not os.path.exists(test_dir):
                    os.mkdir(test_dir)
                self.results[model, test] = result
                if 'figures' in result:
                    self.write_figures(result['figures'], test, model, test_dir)
                self.write_test_result(result, test, model)
                if 'data' in result:
                    pickle.dump(result['data'],
                                open(os.path.join(test_dir, 'data.pkl'), 'wb'),
                                -1)
                self.shelf[key] = result
                self.shelf.sync()
                print 'Completed model {model.id}, test {test.id}'.format(
                                        model=model, test=test)
            self.write_model_results(model)
            model.end()
        self.write_index_pages()

    def write_index_pages(self):
        print 'Writing indices, etc.'
        # write index files etc.
        self.write_index()
        self.write_figure_comparison()
        self.write_figure_table_comparison()
        self.write_table_comparison()
        print 'Finished OK!'
    
    def write_figures(self, figures, test, result, path):
        for figname, (figtitle, fig) in figures.items():
            fname = os.path.join(path, figname)
            fig.savefig(fname+'.png', dpi=self.png_dpi)
            fig.savefig(fname+'.thumb.png', dpi=self.thumbnail_dpi)
            fig.savefig(fname+'.pdf')
            close(fig)
            figures[figname] = (figtitle, None)
    
    def navbar(self, home=False, up=False):
        output = '''
        <div style="background:#aaaaff">
        {content}
        </div>
        '''
        content = []
        if up:
            content.append('<a href="../index.html">up</a>')
        if home:
            content.append('<a href="index.html">home</a>')
        content = ' | '.join(content)
        return output.format(content=content)
    
    def write_test_result(self, result, test, model):
        path = self.paths[model, test]
        template = '''
        <html><head><title>Results for {test_name} on {model_name}</title></head>
        <body>
        {navbar}
        <h1>Model: {model_name}</h1>
        <h1>Test: {test_name}</h1>
        <h2>Results</h2>
        {body}
        <h2>Test details</h2>
        {test_details}
        <h2>Model details</h2>
        {model_details}
        </body>
        </html>
        '''
        model_name = model.name
        model_details = model.description
        test_name = test.name
        test_details = test.description
        fresult = self.format_result(result, test, model)
        body = ''
        for section, title in [('summary', 'Summary'),
                               ('table', 'Table'),
                               ('figures', 'Figures')]:
            if section in fresult:
                body += '<h3>{title}</h3>{content}'.format(title=title,
                                                    content=fresult[section])
        html = template.format(test_name=test_name, model_name=model_name,
                               body=body, model_details=model_details,
                               test_details=test_details,
                               navbar=self.navbar(up=True))
        fname = os.path.join(path, 'index.html')
        open(fname, 'w').write(html)
        
    def write_model_results(self, model):
        template = '''
        <html><head><title>Results for {model_name}</title></head>
        <body>
        {navbar}
        <h1>Model: {model_name}</h1>
        <table width="100%">
        {tableitems}
        </table>
        <h2>Model description</h2>
        {descr}
        </body>
        </html>
        '''
        tableitems = ''
        for test in self.tests:
            test_name = test.name
            result = self.results[model, test]
            testpath = self.paths[model, test]
            test_href = test.uid+'/index.html'
            fresult = self.format_result(result, test, model, test.uid+'/',
                                         thumbnail=True)
            summary = fresult.get('summary', '')
            table = fresult.get('table', '')
            figures = fresult.get('figures', '')
            tableitems += '''
            <tr>
            <td colspan="2" bgcolor="#cccccc">
            <a href="{test_href}"><b>{test_name}</b></a>
            </td>
            </tr>
            <tr>
            <td>
            {summary}
            {table}
            </td>
            <td>
            {figures}
            </td>
            </tr>
            '''.format(test_name=test_name, summary=summary, table=table,
                       figures=figures, test_href=test_href)
        modelpath = self.paths[model]
        fname = os.path.join(modelpath, 'index.html')
        model_name = model.name
        html = template.format(tableitems=tableitems, model_name=model_name,
                               descr=model.description,
                               navbar=self.navbar(up=True))
        open(fname, 'w').write(html)
        
    def write_index(self):
        template = '''
        <html><head><title>Testbed</title></head>
        <body>
        <h1>Comparison pages</h1>
        <ul>
        <li><a href="figcomparison.html">Figure comparison</a></li>
        <li><a href="tablecomparison.html">Table comparison</a></li>
        <li><a href="figtablecomparison.html">Figure and table comparison</a></li>
        </ul>
        <h1>List of models</h1>
        <ul>
        {model_list}
        </ul>
        </body>
        </html>
        '''
        model_list = ''
        for model in self.models:
            href = model.uid+'/index.html'
            model_list += '''
            <li><a href="{href}">{name}</a></li>
            '''.format(href=href, name=model.name)
        html = template.format(model_list=model_list)
        fname = os.path.join(self.results_dir, self.name, 'index.html')
        open(fname, 'w').write(html)

    def write_table_comparison(self):
        template = '''
        <html><head><title>Comparison of tables</title></head>
        <body>
        {navbar}
        <h1>Comparison of tables</h1>
        <table border="1px" bordercolor="#000000" cellspacing="0" cellpadding="5px">
        {table}
        </table>
        </body>
        </html>
        '''
        table = ''
        for test in self.tests:
            test_name = test.name
            table += '''
            <tr>
            <td colspan="{colspan}" bgcolor="#cccccc">
            <b>{test_name}</b>
            </td>
            </tr>
            '''.format(test_name=test_name, colspan=len(self.models)+1)
            result0 = self.results[self.models[0], test]
            if 'table' in result0:
                tab0 = result0['table']
                keys = tab0.keys()
                table += '<tr><td> </td>'
                for model in self.models:
                    table += '<th>'+model.name+'</th>'
                table += '<tr/>'
                for key in keys:
                    table += '<tr>'
                    table += '<th>'+key+'</th>'
                    for model in self.models:
                        result = self.results[model, test]['table']
                        value = result[key]
                        table += '''
                        <td>{value}</td>
                        '''.format(value=value)
                    table += '</tr>'
        fname = os.path.join(self.results_dir, self.name, 'tablecomparison.html')
        html = template.format(table=table,
                               navbar=self.navbar(home=True))
        open(fname, 'w').write(html)

    def write_figure_comparison(self):
        template = '''
        <html><head><title>Comparison of figures</title></head>
        <body>
        {navbar}
        <h1>Comparison of figures</h1>
        <table border="1px" bordercolor="#000000" cellspacing="0" cellpadding="5px">
        {table}
        </table>
        </body>
        </html>
        '''
        table = ''
        colwidth = 100.0/len(self.models)
        for test in self.tests:
            test_name = test.name
            table += '''
            <tr>
            <td colspan="{colspan}" bgcolor="#cccccc" width="100%">
            <b>{test_name}</b>
            </td>
            </tr>
            '''.format(test_name=test_name, colspan=len(self.models))
            table += '<tr>'
            for model in self.models:
                table += '<th width="{colwidth}%">{modelname}</th>'.format(
                                colwidth=colwidth, modelname=model.name)
            table += '<tr/>'
            table += '<tr>'
            for model in self.models:
                result = self.results[model, test]
                base = model.uid+'/'+test.uid+'/'
                fresult = self.format_result(result, test, model, base,
                                             thumbnail=True)
                figures = fresult.get('figures', '')
                table += '''
                <td width="{colwidth}%">{figures}</td>
                '''.format(figures=figures, colwidth=colwidth)
            table += '</tr>'
        fname = os.path.join(self.results_dir, self.name, 'figcomparison.html')
        html = template.format(table=table,
                               navbar=self.navbar(home=True))
        open(fname, 'w').write(html)

    def write_figure_table_comparison(self):
        colwidth = 100.0/len(self.models)
        template = '''
        <html><head><title>Comparison of figures and tables</title>
        <script type="text/javascript">
        function toggle(cls) {{
            var elements = document.getElementsByTagName('*');
            for(i=0; i<elements.length; i++) {{
                if(elements[i].className == cls) {{
                    if(elements[i].style.display == "") {{
                        elements[i].style.display = "none";
                    }} else {{
                        elements[i].style.display = "";
                    }}
                }}
            }}
        }}
        </script>
        </head>
        <body>
        {navbar}
        <h1>Comparison of figures and tables</h1>
        <ul>
        {restore}
        </ul>
        <table border="1px" bordercolor="#000000" cellspacing="0" cellpadding="5px">
        {table}
        </table>
        </body>
        </html>
        '''
        restore_element = '''
        <li class="{cls}" style="display: none;">
        <a href="javascript: toggle('{cls}')">
        Restore {name}
        </a></li>
        '''
        restore = [restore_element.format(cls=model.uid,
                                  name=model.name) for model in self.models]
        restore = '\n'.join(restore)
        table = '''
        <tr>
        <th colspan="{colspan}" bgcolor="#cccccc">
        <a href="javascript: toggle('modeldesc')">
        Model descriptions
        </a>
        </th>
        </tr>
        '''.format(colspan=len(self.models))
        table += '<tr class="modeldesc">'
        modelheaders = ''
        for model in self.models:
            modelheaders += '''
            <th class="{cls}">
            <a href="javascript: toggle('{cls}')">
            {name}
            </a></th>
            '''.format(cls=model.uid, name=model.name)
        table += modelheaders
        table += '</tr>'
        table += '<tr class="modeldesc">'
        for model in self.models:
            table += '''
            <td class="{cls}">{desc}</td>
            '''.format(desc=model.description, cls=model.uid)
        table += '</tr>'
        for test in self.tests:
            test_name = test.name
            table += '''
            <tr>
            <td colspan="{colspan}" bgcolor="#cccccc" width="100%">
            <b>
            <a href="javascript: toggle('{cls}')">
            {test_name}
            </a>
            </b>
            </td>
            </tr>
            '''.format(test_name=test_name, colspan=len(self.models),
                       cls=test.uid)
            table += '<tr class="{cls}">'.format(cls=test.uid)
            table += modelheaders
            table += '</tr>'
            thefigures = {}
            thetables = {}
            inctables = False
            for model in self.models:
                result = self.results[model, test]
                base = model.uid+'/'+test.uid+'/'
                fresult = self.format_result(result, test, model, base,
                                             thumbnail=True)
                thefigures[model] = fresult.get('figures', '')
                ftable = fresult.get('table', '')
                thetables[model] = ftable
                if ftable:
                    inctables = True
            table += '<tr class="{cls}">'.format(cls=test.uid)
            for model in self.models:
                table += '''
                <td width="{colwidth}%" class="{cls}">{figures}</td>
                '''.format(figures=thefigures[model], colwidth=colwidth,
                           cls=model.uid)
            table += '</tr>'
            if inctables:
                table += '<tr class="{cls}">'.format(cls=test.uid)
                for model in self.models:
                    table += '''
                    <td width="{colwidth}%" class="{cls}">{tables}</td>
                    '''.format(tables=thetables[model], colwidth=colwidth,
                               cls=model.uid)
                table += '</tr>'
        fname = os.path.join(self.results_dir, self.name, 'figtablecomparison.html')
        html = template.format(table=table, restore=restore,
                               navbar=self.navbar(home=True))
        open(fname, 'w').write(html)
            
    def format_result(self, result, test, model, figbase='', thumbnail=False):
        output = {}
        # insert figures
        insert_fig_link = thumbnail
        if thumbnail:
            thumbnail_ext = '.thumb'
        else:
            thumbnail_ext = ''
#        insert_fig_link = imgwidth is not None
#        if imgwidth is None:
#            imgwidth = ''
#        else:
#            imgwidth = 'width="{imgwidth}px"'.format(imgwidth=imgwidth)
        if 'summary' in result:
            output['summary'] = '''
            <div>
            {summary}
            </div>
            '''.format(summary=result['summary'])
        if 'figures' in result and len(result['figures']):
            figoutput = ''
            figures = result['figures']
            keys = result.get('figures_order', figures.keys())
            #for figname, (figtitle, fig) in figures.items():
            for figname in keys:
                figtitle, fig = figures[figname]
                fname = figbase+figname+'.png'
                imgfname = figbase+figname+thumbnail_ext+'.png'
                img_code = '<img src="{imgfname}"/>'
                if insert_fig_link:
                    img_code = '<a href="{href}">'+img_code+'</a>'
                figoutput += ('''
                <p align="center">'''+img_code+'''
                <br/>
                <b>Figure {figname}: {figtitle}</b>
                </p>
                ''').format(fname=fname, figname=figname, figtitle=figtitle,
                           img_code=img_code,
                           href=fname, imgfname=imgfname)
            output['figures'] = figoutput
        if 'table' in result and len(result['table']):
            table = result['table']
            tbloutput = '''
            <table border="1px" bordercolor="#000000" cellspacing="0" cellpadding="5px" width="100%">
            <tr>
                <th>Name</th>
                <th>Value</th>
            </tr>
            '''
            for name, value in table.iteritems():
                tbloutput += '''
                <tr>
                <td>{name}</td>
                <td>{value}</td>
                </tr>
                '''.format(name=name, value=value)
            tbloutput += '</table>'
            output['table'] = tbloutput
        return output
