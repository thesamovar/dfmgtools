'''
You can use the AxisRecorder class to behave precisely as an axis, but also allow replaying later.
'''

__all__ = ['AxisRecorder']

class CallCatcher(object):
    def __init__(self, orig, name, calls):
        self.orig = orig
        self.name = name
        self.calls = calls
    def __call__(self, *args, **kwds):
        self.calls.append((self.name, args, kwds))
        return self.orig(*args, **kwds)

class AxisRecorder(object):
    def __init__(self, obj):
        self._ax_record_obj = obj
        self._ax_record_calls = []
    def __getattr__(self, name):
        a = getattr(self._ax_record_obj, name)
        if callable(a):
            return CallCatcher(a, name, self._ax_record_calls)
        else:
            return a
    def _ax_record_replotter(self):
        return AxisRecordReplotter(self._ax_record_calls)
    
class AxisRecordReplotter(object):
    def __init__(self, calls):
        self.calls = calls
    def __call__(self, ax):
        for name, args, kwds in self.calls:
            getattr(ax, name)(*args, **kwds)
