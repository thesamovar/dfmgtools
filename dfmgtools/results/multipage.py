from pylab import *
from matplotlib.backends.backend_pdf import PdfPages

__all__ = ['plot_multipage_pdf']

def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

def plot_multipage_pdf(filename, plot_func, args,
                       pagesize=(8.3, 11.7),
                       subplot_layout=(4, 2),
                       top=0.92,
                       ax_title=None,
                       page_title=None,
                       suppress_axis_labels=True,
                       sharex=True,
                       sharey=True,
                       suppress_legends=True,
                       ):
    if suppress_legends:
        args = [args[0]]+args
    sub_h, sub_w = subplot_layout
    pages = chunks(list(enumerate(args)), sub_w*sub_h)
    pp = PdfPages(filename)
    figs = []
    for pagenum, page in enumerate(pages):
        fig = figure(figsize=pagesize)
        figs.append(fig)
        for i, arg in page:
            pos_y = (i%(sub_w*sub_h))/sub_w
            pos_x = (i%(sub_w*sub_h))%sub_w
            kwds = {}
            if sharex and i>suppress_legends:
                kwds['sharex'] = ax
            if sharey and i>suppress_legends:
                kwds['sharey'] = ax
            ax = subplot(sub_h, sub_w, i%(sub_w*sub_h)+1, **kwds)
            if callable(ax_title):
                cur_ax_title = ax_title(arg)
            elif ax_title is not None:
                cur_ax_title = ax_title+' (%s of %s)' % (i+1, len(args))
            try:
                plot_func(arg, i, pos_x, pos_y, sub_h, sub_w)
            except TypeError:
                plot_func(arg)
            if suppress_legends:
                leg = ax.legend()
                if leg is not None:
                    if i==0:
                        ax.clear()
                        ax.set_frame_on(False)
                        ax.set_xticks([], [])
                        ax.set_yticks([], [])
                        ax.add_artist(leg)
                    else:
                        if leg is not None:
                            ax.legend().set_visible(False)
            if ax_title is not None:
                title(cur_ax_title)
            if suppress_axis_labels:
                if pos_x!=0:
                    ylabel('')
                if pos_y!=sub_h-1:
                    xlabel('')
        if page_title is None:
            cur_page_title = 'Page %s of %s' % (pagenum+1, len(pages))
        else:
            cur_page_title = page_title+' (%s of %s)' % (pagenum+1, len(pages))
        suptitle(cur_page_title)
        fig.tight_layout()
        fig.subplots_adjust(top=top)
        
    for fig in figs:
        pp.savefig(fig)
        
    pp.close()
    
    for fig in figs:
        close(fig)
