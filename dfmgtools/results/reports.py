'''
Module for generating reports of simulations, data, etc.

Handles the following:

- Categorised data, e.g. model/test
   . Figures (full size and thumbnail) and (formatted) text
- Multiple views of the data, with links to go to a page with more details, etc.
   . Hierarchical list view
   . Table view

Categorisation of data is handled by names which are tuples, e.g.
('modelname', 'testname', 'figures', 'figurename')
'''

from pylab import *
from StringIO import StringIO
from dfmgtools.core.paths import *
import os
import re
from collections import defaultdict

__all__ = ['Figure', 'TableRow', 'TableHeaderRow', 'Table', 'Section', 'Page',
           'link', 'List', 'UnorderedList', 'OrderedList', 'Report',
           'parameter_table', 'Sequence', 'Anchor', 'anchor',
           ]

def parse_name(name):
    if isinstance(name, str):
        return tuple(name.split('/'))
    else:
        return tuple(name)


def save_figure_to_string(fig, format='png', dpi=150):
    buf = StringIO()
    fig.savefig(buf, format=format, dpi=dpi)
    return buf.getvalue()


def link(text, item):
    if isinstance(item, str):
        url = item
    else:
        url = item.url
    return '<a href="{url}">{text}</a>'.format(url=url, text=text)


def anchor(name):
    return '<a name="%s"></a>' % name


def parameter_table(params):
    table = '<table border="1px" bordercolor="#000000" >'
    for k, v in params.iteritems():
        table += '<tr><th>'+k+'</th><td>'+str(v)+'</td></tr>\n'
    table += '</table>'
    return table


def parse_to_sequence(items):
    if isinstance(items, (tuple, list)):
        return Sequence(items)
    else:
        return items


class Figure(object):
    def __init__(self, name, fig, dpi=150, thumbnail_dpi=50, caption=True):
        self.name = name = parse_name(name)
        self.title = 'Figure '+name[-1]
        self.fullsize_png = save_figure_to_string(fig, 'png', dpi)
        self.thumbnail_png = save_figure_to_string(fig, 'png', thumbnail_dpi)
        self.pdf = save_figure_to_string(fig, 'pdf', dpi)
        self.url_base = '/'.join(name)
        self.url_fullsize_png = self.url_base+'.png'
        self.url_thumbnail_png = self.url_base+'.thumbnail.png'
        self.url_pdf = self.url_base+'.pdf'
        self.html_fullsize = '''
            <a href="{self.url_pdf}">
                <img src="{self.url_fullsize_png}">
            </a>
            '''.format(self=self)
        self.page = Page(name, 'Figure '+name[-1], [
            'Full name of figure: '+'/'.join(name),
            '<a href="{self.url_fullsize_png}">Full size image</a>'.format(self=self),
            '<a href="{self.url_pdf}">PDF version</a>'.format(self=self),
            self.html_fullsize])
        self.url = self.page.url
        if caption:
            self.html_thumbnail = '''
                <div>
                    <a href="{self.page.url}">
                        <img src="{self.url_thumbnail_png}">
                    </a>
                    <br/>
                    <b>Figure {lastname}</b>
                    (<a href="{self.url_fullsize_png}">larger</a>, <a href="{self.url_pdf}">pdf</a>)
                </div>
                '''.format(self=self, lastname=name[-1])
        else:
            self.html_thumbnail = '''
                    <a href="{self.page.url}">
                        <img src="{self.url_thumbnail_png}">
                    </a>
                '''.format(self=self, lastname=name[-1])
        
    def write(self, basedir):
        ensure_directory_of_file(os.path.join(basedir, self.url_fullsize_png))
        open(os.path.join(basedir, self.url_fullsize_png), 'wb').write(self.fullsize_png)
        open(os.path.join(basedir, self.url_thumbnail_png), 'wb').write(self.thumbnail_png)
        open(os.path.join(basedir, self.url_pdf), 'wb').write(self.pdf)
        self.page.write(basedir)
        
    def __str__(self):
        return self.html_thumbnail
    __repr__ = __str__


class TableRow(object):
    def __init__(self, items):
        if not isinstance(items, (list, tuple)):
            items = [items]
        self.items = [parse_to_sequence(item) for item in items]
    itemtag = 'td'
    rowopts = ''
    cellopts = 'valign="top"'

class TableHeaderRow(TableRow):
    itemtag = 'th'
    rowopts = 'bgcolor="#cccccc"'
    cellopts = ''
        
class Table(object):
    def __init__(self, rows):
        self.rows = []
        for row in rows:
            self.add(row)
        
    def add(self, row):
        if isinstance(row, (list, tuple)):
            row = TableRow(row)
        self.rows.append(row)

    def __str__(self):
        self.num_rows = len(self.rows)
        self.num_cols = amax([len(row.items) for row in self.rows])

        s = '<table width="100%" border="1px" bordercolor="#000000">\n'
        for row in self.rows:
            s += '<tr {rowopts}>\n'.format(rowopts=row.rowopts)
            itemtag = row.itemtag
            items = row.items
            if len(items)==1:
                s += '''
                <{itemtag} {cellopts} colspan="{num_cols}" bgcolor="#aaaaaa">
                {item}
                </{itemtag}>
                '''.format(itemtag=itemtag, num_cols=self.num_cols,
                           item=str(items[0]), cellopts=row.cellopts)
            else:
                for item in items:
                    s += '''
                    <{itemtag} {cellopts}>
                    {item}
                    </{itemtag}>
                    '''.format(itemtag=itemtag, item=str(item),
                               cellopts=row.cellopts)
            s += '</tr>\n'
        s += '</table>\n'
        return s
    __repr__ = __str__


class Anchor(object):
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return '<a name="#%s"></a>' % self.name
    __repr__ = __str__


class List(object):
    tag = 'ul'
    def __init__(self, items):
        self.items = [parse_to_sequence(item) for item in items]
        
    def __str__(self):
        body = '\n'.join('<li>'+str(item)+'</li>' for item in self.items)
        html = '''
            <{tag}>
                {body}
            </{tag}>
            '''.format(tag=self.tag, body=body)
        return html
    __repr__ = __str__

class UnorderedList(List):
    pass

class OrderedList(List):
    tag = 'ol'


class Section(object):
    def __init__(self, title, contents):
        self.title = title
        if not isinstance(contents, (list, tuple)):
            contents = [contents]
        contents = [parse_to_sequence(item) for item in contents]
        self.contents = contents
        self.anchor = re.sub(r'[^a-zA-Z0-9]', '', self.title)
        
    def add(self, item):
        self.contents.append(item)
    
    def html(self, level=1, basenumber=(), number=1, numbering=False):
        body = ''
        mynumber = basenumber+(number,)
        nextnumber = 1
        for item in self.contents:
            if isinstance(item, Section):
                body += item.html(level=level+1, basenumber=mynumber,
                                  number=nextnumber, numbering=numbering)
                nextnumber += 1
            elif isinstance(item, str):
                body += '<div>'+item+'</div>'
            elif isinstance(item, (list, tuple)):
                body += str(Sequence(item))
            else:
                body += str(item)
        if numbering:
            num = '.'.join(map(str, mynumber))+'. '
        else:
            num = ''
        return '''
            <a name="{anchor}"/>
            <h{level}>
                {num}{title}
                <a class="anchorlink" href="#{anchor}">(link)</a>
            </h{level}>
            {body}
            '''.format(level=level, title=self.title, body=body, num=num,
                       anchor=self.anchor)
    
    def __str__(self):
        return self.html()
    __repr__ = __str__
    

class Sequence(object):
    def __init__(self, items):
        self.items = [parse_to_sequence(item) for item in items]
    
    def __str__(self):
        return '\n'.join('<div>'+str(item)+'</div>' for item in self.items)
    __repr__ = __str__


class Page(object):
    def __init__(self, name, title, contents, numbering=False):
        self.name = name = parse_name(name)
        self.title = title
        self.numbering = numbering
        self.url = '/'.join(name)+'.html'
        if not isinstance(contents, (list, tuple)):
            contents = [contents]
        self.contents = []
        self.add(contents)
        
    def add(self, items):
        if not isinstance(items, (list, tuple)):
            items = [items]
        for item in items:
            if isinstance(item, (list, tuple)):
                item = Sequence(item)
            self.contents.append(item)
            
    def write(self, basedir):
        fname = os.path.join(basedir, self.url)
        ensure_directory_of_file(fname)
        open(fname, 'w').write(str(self))

    def __str__(self):
        body = ''
        number = 1
        for item in self.contents:
            if isinstance(item, str):
                item = '<div>'+item+'</div>'
            elif isinstance(item, Section):
                item = item.html(number=number, numbering=self.numbering)
                number += 1
            body += str(item)
        style = '''
            .anchorlink
            {
                color: #cccccc;
                text-decoration: none;
                font-size: 50%;
            }
            .anchorlink:hover
            {
                color: #0000aa;
            }
            
            .navlink
            {
                color: #cccccc;
                text-decoration: none;
            }
            .navlink:hover
            {
                color: #0000aa;
            }
            '''
        thisindexbase = '/'.join(self.name[:-1])
        if thisindexbase=='':
            thisindexbase = '.'
        upindexbase = '/'.join(self.name[:-2])
        if upindexbase=='':
            upindexbase = '.'
        html = '''
            <html>
                <head>
                    <title>{self.title}</title>
                    <base href="{base}"/>
                    <style type="text/css">
                        {style}
                    </style>
                </head>
                <body>
                    <div style="color: #aaaaaa">
                    <a href="{thisindex}" class="navlink">Category index</a> |
                    <a href="{upindex}" class="navlink">Parent index</a>
                    </div>
                    {body}
                </body>
            </html>
            '''.format(self=self, body=body, base='../'*(len(self.name)-1),
                       style=style,
                       thisindex=thisindexbase+'/index.html',
                       upindex=upindexbase+'/index.html',
                       )
        return html
    __repr__ = __str__


class Report(object):
    def __init__(self, *items):
        self.items = {}
        self.graph = defaultdict(set)
        self.add(items)
        self.index_pages = {}
    
    def add(self, items):
        if not isinstance(items, (list, tuple)):
            items = [items]
        for item in items:
            if item.name in self.items:
                raise ValueError("Item "+str(item.name)+" name clash")
            self.items[item.name] = item
            t = ()
            for n in item.name:
                self.graph[t].add(n)
                t = t+(n,)
    
    def compute_index_pages(self):
        if len(self.index_pages):
            return
        for basecat, subcats in self.graph.items():
            catname = '/'.join(basecat)
            page = Page(basecat+('index',),
                        'Index for '+catname,
                        []
                        )
            self.index_pages[basecat] = page
        # Add pages to indices
        def besttitle(item, lastname):
            if hasattr(item, 'title'):
                return item.title
            else:
                return lastname
        for basecat, subcats in self.graph.items():
            subcatlinks = [link(besttitle(self.items[basecat+(n,)], n),
                                self.items[basecat+(n,)]) for n in subcats if basecat+(n,) in self.items]
            if len(subcatlinks):
                list = List(subcatlinks)
                self.index_pages[basecat].add('Pages')
                self.index_pages[basecat].add(list)
        # Add subcategories to indices
        for basecat, subcats in self.graph.items():
            subcatlinks = [link(n, self.index_pages[basecat+(n,)]) for n in subcats if basecat+(n,) in self.index_pages]
            if len(subcatlinks):
                list = List(subcatlinks)
                self.index_pages[basecat].add('Sub-categories')
                self.index_pages[basecat].add(list)
                
    def write(self, basedir):
        # write user items
        for v in self.items.values():
            v.write(basedir)
        # write indices
        self.compute_index_pages()
        for page in self.index_pages.values():
            page.write(basedir)


if __name__=='__main__':
    f = figure()
    plot(rand(50))
    F = Figure('testcat/test', f)
    table = Table([
        TableHeaderRow('Main header'),
        TableHeaderRow(['Col 1', 'Col 2']),
        ['Some text', F],
        [F, 'Some other text with a '+link('link', F)+'.'],
        ])
    page1 = Page('test', 'Test title', [
        Section('Main page', [
            'Some test text.',
            Section('Subsection', [
                ['Some more.', 'Oh yes.'],
                ]),
            ]),
        Section('Tables and stuff', [                    
            table,
            F,
            'Final test text.',
            ]),
        ], numbering=True)
    page2 = Page('anothercat/test2', 'Test title 2', [
        'Nothing to see here except this list.',
        List(['Item 1',
              'Item 2',
              link('Link to testcat index', 'testcat/index.html'),
              ]),
        ])
    page1.add(link('Page 2', page2))
    report = Report(page1, page2, F)
    report.write('testdir')
