from pylab import *

__all__ = ['auto_subplot_dimensions']

def auto_subplot_dimensions(n, aspect_min=0.65, prefer_horizontal=False):
    '''
    Returns a pair (numrows, numcols) of subplot dimensions to fit n plots with a minimum amount of wasted
    space subject to the constraint that the aspect is between ``aspect_min`` and ``1/aspect_min``.
    Normally, vertical orientations are preferred but if horizontal ones are desired, set
    ``prefer_horizontal=True``. 
    '''
    possibilities = []
    for w in xrange(1, n+1):
        h = int(ceil(n*1.0/w))
        asp = w*1.0/h
        if asp>1:
            asp = 1/asp
        if asp<aspect_min:
            continue
        if w>h and prefer_horizontal:
            continue
        elif w<h and not prefer_horizontal:
            continue
        wasted = w*h-n
        possibilities.append((w, h, wasted))
    if not possibilities:
        n = int(ceil(sqrt(n)))
        possibilities = [(n, n, 0)]
    w, h, wasted = sorted(possibilities, key=lambda (w, h, wasted): wasted)[0]
    return w, h

if __name__=='__main__':
    for n in xrange(1, 15):
        w, h = auto_subplot_dimensions(n)
        figure()
        for i in xrange(1, n+1):
            subplot(w, h, i)
    show()
    