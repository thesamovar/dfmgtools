from pylab import *
import matplotlib

__all__ = ['plot_superimposed_curves']

# I got this from somewhere on the internet
def figure_to_array ( fig ):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw ( )
 
    # Get the RGB buffer from the figure
    w,h = fig.canvas.get_width_height()
    buf = fromstring ( fig.canvas.tostring_rgb(), dtype=uint8 )
    buf.shape = ( h, w, 3)
 
    return buf

def plot_superimposed_curves(x, curves, color='k', ax=None, dpi=80, figsize=(8, 6), **kwds):
    plot_ax = ax
    curves = array(curves)

    ymin = amin(curves)
    ymax = amax(curves)
    
    img = None
    
    f = figure(frameon=False, dpi=dpi, figsize=figsize)
    ax =  f.add_axes([0,0,1,1])
    for s in ax.spines.values():
        s.set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(amin(x), amax(x))
    ax.set_ylim(ymin, ymax)
    
    for i, curve in enumerate(curves):
        ax.cla()
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlim(amin(x), amax(x))
        ax.set_ylim(ymin, ymax)
        ax.plot(x, curve, '-k', **kwds)
        curve_img = 1-figure_to_array(f)[:, :, 0]*1.0/255.0
        if img is None:
            img = curve_img*1.0
        else:
            img += curve_img
    
    close(f)
    
    img /= amax(img)
    
    color = matplotlib.colors.ColorConverter().to_rgb(color)
    color = array(color)[newaxis, newaxis, :]
    img = dstack((img,)*3)    
    img = img*(1-color)
    img = 1-img
    
    extent = (amin(x), amax(x), ymin, ymax)

    if plot_ax is not False:
        if plot_ax is None:
            plot_ax = gca()
        plot_ax.imshow(img, aspect='auto', extent=extent)
    
    return img, extent

if __name__=='__main__':
    
    x = linspace(-1, 1, 100)
    y = x**2
    
    curves = []
    for _ in xrange(100):
        z = y+1.5*(x[1]-x[0])*cumsum(randn(len(x)))
        curves.append(z)
    curves = array(curves)
    
    ymin = amin(curves)
    ymax = amax(curves)
    
    img, extent = plot_superimposed_curves(x, curves, color='r', lw=5, dpi=160)
    print img.shape
    plot(x, mean(curves, axis=0), '-k')
    
    show()
    
        