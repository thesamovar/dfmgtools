from dfmgtools import *
from dfmgtools.compute.auditory.models.zilanycarneyjasa2009 import *
import time

if __name__=='__main__':
    model = CatANModel()
    model.start() # sets samplerate
    sound = tone(1*kHz, 90*ms).atlevel(40*dB).shifted(10*ms)
    
    #cfs = erbspace(500*Hz, 2*kHz, 2000)
    cfs[:] = 1*kHz # model is optimised for this case
    
    start = time.time()
    i, t = model.run(sound, len(cfs)/sound.nchannels, cfs, {})
    print 'Time', time.time()-start
    plot(t, i, '.k')
    show()
    