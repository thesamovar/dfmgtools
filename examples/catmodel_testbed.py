import matplotlib
matplotlib.use('Agg')

from dfmgtools import *
from dfmgtools.results.auditory.testbed import *
from dfmgtools.compute.auditory.models.zilanycarneyjasa2009 import *
from dfmgtools.compute.auditory.tests import *

models = [CatANModel()]

testbed = Testbed(models, standard_monaural_tests,
                  name='catmodel_testbed',
                  results_dir='.',
                  )

if __name__=='__main__':
    #testbed.clean()
    testbed.run()
