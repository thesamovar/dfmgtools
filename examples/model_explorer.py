from dfmgtools import *
from dfmgtools.gui.model_explorer import *
import time

slow_mode = False

class SampleModel(ExplorableModel):
    explorer_type = 'sample_model_explorer'
    plot_styles = ['b', 'g', 'r']
    param_specs = [Parameter('n', 1, 1, 10, 1),
                   Parameter('freq', 2*Hz, 1*Hz, 10*Hz, 1*Hz, unit=kHz),
                   Parameter('phase', 0, 0, 360, 15),
                   Parameter('fake', 0, 0, 100, 1),
                   ]

    def get_data(self, freq, phase, n, fake=None):
        t = linspace(0, 1, 10000)
        if not slow_mode:
            y = sin(2*pi*freq*t+phase*pi/180.)**n
        else:
            y = zeros_like(t)
            for i in xrange(len(t)):
                self.update(float(i)/len(t))
                y[i] = sin(2*pi*freq*t[i]*second+phase*pi/180.)**n
        return t, y
    
    def plot_data(self, fig, style, (t, y)):
        fig.clear()
        ax = fig.add_subplot(111)
        ax.plot(t, y, c=style)

if __name__=='__main__':
    SampleModel().launch_gui()
    
